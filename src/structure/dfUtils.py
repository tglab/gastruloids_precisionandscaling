import os, sys
import numpy as np
import pandas as pd


def changedtypes(df, dict_dtypes):
    #TODO: correct for the case when changing for int and it is not a number in the column ?
    # Because 0.0 for not number is not the best solution. Would prefere pd.NA
    df.fillna(0.0, inplace=True)
    for d in dict_dtypes:
        if d in df:
            df[d] = df[d].astype(dict_dtypes[d])
    return df

def merge_multkeys(df1, df2, **kargs):

    commonkeys = df1.columns.intersection(df2.columns).tolist()
    df= pd.merge(df1,df2, on=commonkeys, **kargs)

    return df


def defineOutterWells(df_analysis):
    '''Assign a value InOut at each row of a dataframe to separate gastruloids that were in the outter part of a plate
    df_analysis: dataframe on which to append the value InOut

    return: the updated dataframe '''

    #TODO: move in another module

    if not ('Row' in df_analysis) or not ('Col' in df_analysis):
        print('Row and Col values not defined in the dataframe.')
    else:
        df_analysis['InOut'] = 'None'
        for i in df_analysis.index:
            row = df_analysis.loc[i]
            if row['Row'] == 1 or row['Row'] == 8 or row['Col']==1 or row['Col']==12:
                df_analysis.at[i,'InOut'] = 'Outter'
            else:
                df_analysis.at[i,'InOut'] = 'Inner'

    return df_analysis

