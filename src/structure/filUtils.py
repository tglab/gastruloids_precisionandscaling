import os


def strfind_between(s, sbeg, send):
    '''Find caracters comprised between two motifs in a string
    s: string to look at
    sbeg: motif that is before
    send: motif that is after

    return: the string between sbeg and send'''

    #TODO: implement the case where either sbeg or send is found several times in s

    start = s.find(sbeg) + len(sbeg)
    end = s.find(send)

    return s[start:end]

def figsave(folder, fig, name, dpi=600):

    '''To save a figure under a .jpg depending on the date it is produced:
    - folder: the folder in which to save the figure. A subfolder with today date will be created if doesn't exist.
    - fig: the figure object to save
    - name: the name of the .jpg file
    - dpi: dots per inch, by default 600 '''
    #TODO: implement the automation of this function with a fixed folder in global_structure

    from datetime import date

    todayfolder = os.path.join(folder,date.today().strftime("%Y%m%d"))
    if not os.path.exists(todayfolder):
        os.mkdir(todayfolder)
    filename = os.path.join(todayfolder, name)
    k=0
    while os.path.exists( filename):
        filename = filename + str(k).zfill(1)
    fig.savefig(filename + '.jpg', dpi =dpi )



