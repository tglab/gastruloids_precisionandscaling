
def div(d,dv):
    if dv==0:
        return 0
    else:
        return d/dv

def dividelists(data, datadiv):
    try:
        return [div(d,dv) for (d,dv) in zip(data,datadiv)]
    except:
        return [0 for d in data]