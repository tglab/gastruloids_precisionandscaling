import os, sys
import numpy as np
import pandas as pd

import src.structure.filUtils as filu
import src.image.imUtils as imu

from openpyxl import load_workbook

pd.set_option('display.max_columns', 500)
pd.set_option('display.max_colwidth',20)
pd.set_option('display.max_rows', 10)
pd.options.mode.chained_assignment = None

# import matplotlib.pyplot as plt

# from melo_fun import ImUtils as imu
# import imutils
# import tifffile as tifff

ANALYSIS_NAME = ''
DATA_FOLDER = '.'
DATA_ext = ''
RES_FOLDER = '.'

GLOBAL_INFOS = '/mnt/pbf1/Melody/gastruloids_platesinfos.ods'
PLATE_NCELLS = '/mnt/pbf1/Melody/gastruloids_ncells.ods'

INIT = False

## Global variables

def define_data_folder( folder, silence=False):

    global DATA_FOLDER
    DATA_FOLDER = folder

    if not silence:
        print("Data folder defined as : " , DATA_FOLDER)

    global INIT
    INIT = False

    return DATA_FOLDER

def define_results_folder( folder, silence=False):

    global RES_FOLDER
    RES_FOLDER = folder

    if not silence:
        print("Result folder defined as : " , RES_FOLDER)

    if not os.path.exists(RES_FOLDER):
        os.mkdir(RES_FOLDER)
        print('Result folder did not exist. Has been created')

    if not os.path.exists(os.path.join(RES_FOLDER, ANALYSIS_NAME)):
        os.mkdir( os.path.join(RES_FOLDER, ANALYSIS_NAME))
        print('Analysis folder did not exist. Has been created')

    global INIT
    INIT = False

    return RES_FOLDER


def define_analysis_name( name, silence=False):

    global ANALYSIS_NAME
    ANALYSIS_NAME = name

    if not silence:
        print("Analysis name defined as : " , ANALYSIS_NAME)

    if not os.path.exists(RES_FOLDER):
        os.mkdir(RES_FOLDER)
        print('Result folder did not exist. Has been created')

    if not os.path.exists(os.path.join(RES_FOLDER, ANALYSIS_NAME)):
        os.mkdir( os.path.join(RES_FOLDER, ANALYSIS_NAME))
        print('Analysis folder did not exist. Has been created')

    global INIT
    INIT = False

    return ANALYSIS_NAME

def define_data_ext( ext):

    global DATA_ext
    DATA_ext = ext

    print('Data files extension defined as: ', DATA_ext)

    global INIT
    INIT = False

    return DATA_ext


def define_global_infos(filename):

    global GLOBAL_INFOS
    GLOBAL_INFOS = filename

    print('File containing global infos about the plates defined as ', GLOBAL_INFOS)

    global INIT
    INIT = False

    return GLOBAL_INFOS

def define_ncellsinfos(filename):

    global PLATE_NCELLS
    PLATE_NCELLS = filename

    print('File containing global infos about the initial number of cells', PLATE_NCELLS)

    global INIT
    INIT = False

    return PLATE_NCELLS



## Dataframe summarizing plates

def initiate_plates_summary( plate_categories=[]):

    if not os.path.exists(os.path.join(RES_FOLDER, ANALYSIS_NAME)):
        print('Result folder does not exist, please initialize analysis.' )

    list_folders  = os.listdir(DATA_FOLDER)

    if plate_categories :
        print('list of plate categories given: ', plate_categories)
        abs_path_folders = [os.path.join(DATA_FOLDER, f) for f in list_folders if f.startswith(tuple(plate_categories))]
    else:
        abs_path_folders = [os.path.join(DATA_FOLDER, f) for f in list_folders]

    df_summ = pd.DataFrame(columns=['Plate', 'Time', 'Date', 'Folder'])

    for i,a in enumerate(abs_path_folders):
        plate_name= os.path.basename(abs_path_folders[i])
        list_subfolders = os.listdir(a)
        for j,sb in enumerate(list_subfolders):
            date = filu.strfind_between(sb, '' , '_G')
            time = filu.strfind_between(sb, plate_name+'_', 'h_')
            s = pd.Series([ plate_name, time, date, list_subfolders[j]], index=df_summ.columns)
            df_summ = df_summ.append(s, ignore_index=True)

    saveSummary(df_summ)

    global INIT
    INIT = True

    return df_summ

def create_subfolders():

    if not INIT:
        sys.exit("!Error!: The summary dataframe has not be initialized. Please initialize."
                 "\nRES_FOLDER : "+ RES_FOLDER +
                 "\nANALYSIS_NAME : " + ANALYSIS_NAME)

    df_summ = loadSummary()

    for id in df_summ["Plate"]:
        if not os.path.exists( os.path.join(RES_FOLDER, ANALYSIS_NAME, id) ):
            os.mkdir( os.path.join(RES_FOLDER, ANALYSIS_NAME, id))

def verify_init():

    if not INIT:
        print( 'Data analysis not initialized. Code stopping now, please inititate.')
        sys.exit()

    return

def loadSummary( ):

    df_summ= pd.read_csv(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_summary.csv'))

    return df_summ

def saveSummary( df_summ ):

    df_summ.to_csv(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_summary.csv'), index=False)

    return df_summ

def correct_times(timepoints=[24,48,72,96,120]):

    verify_init()

    df_summ = loadSummary()
    for i,time in enumerate(df_summ['Time']):
        df_summ.at[i, 'Time'] = timepoints[ np.argmin(np.abs([time-t for t in timepoints]))]

    saveSummary(df_summ)
    return df_summ

def read_dimensions( keys = {'xSize': 'Nx', 'ySixe': 'Ny', 'Scale_pu': 'dx'}):

    verify_init()

    df_summ=loadSummary()
    for k in keys.keys():
        df_summ[k] =''

    for i in range(df_summ.shape[0]):
        folder = os.path.join(  DATA_FOLDER, df_summ.at[i, "Plate"], df_summ.at[i, "Folder"])
        for f in os.listdir( folder ):
                _, ext = os.path.splitext( f)
                if ext == DATA_ext:
                    meta = imu.get_metadata( os.path.join(folder, f) )
                    for k in keys.keys():
                        df_summ.at[i, k] =  meta[keys[k]]
                    break

    saveSummary(df_summ)

    return df_summ

def read_ncells(filepath, name='results'):

    #TODO: change the initialization so that the number of cells is already present before analysis

    df_ncells = pd.read_excel(filepath, dtype={'Plate': object, 'Well': object, 'Row': object, 'Col': object, 'nCells':np.int})
    df_analysis = load_data_dataframe(name=name)

    df_analysis = pd.merge(left=df_analysis, right=df_ncells, left_on =['Plate', 'Well', 'Row','Col'], right_on =['Plate', 'Well', 'Row','Col'], how='left')

    return df_analysis

def plate2wellist(df_plate, plateID, colname = 'info'):

    df_wells = pd.DataFrame(columns={'Plate', 'Well','Row', 'Col',  colname})
    for i,row in df_plate.iterrows():
        for j in range(1, len(row)):
            df_wells = df_wells.append({'Plate': plateID, 'Well': str(row[plateID]) +str(j), 'Row': i+1, 'Col': j, colname: row[j]}, ignore_index=True)

    return df_wells


## Dataframe containing analysis

def initiate_analysis_dataframe(plates = [], plate_type  = '96wells'):

    df_analysis = pd.DataFrame(columns=['Plate', 'Time', 'Well', 'Row', 'Col'])
    df_well = pd.read_csv('../src/infos/'+ plate_type + '.csv', dtype=str)

    if not plates:
        df_summ = loadSummary()
        plates = df_summ.groupby(['Plate', 'Time', 'Folder']).size().reset_index()

    for i in range(plates.shape[0]):
        df_well_app = df_well.copy()
        df_well_app.insert(0, 'Plate', plates.at[i, 'Plate'])
        df_well_app.insert(1, 'Time', plates.at[i, 'Time'])
        df_well_app.insert(2, 'Folder', plates.at[i, 'Folder'])
        df_analysis = df_analysis.append(df_well_app)

    print(df_analysis)
    df_analysis.insert(0, 'Flag', 'Initiation')
    # df_analysis.insert(0, 'imageID', df_analysis['Plate'].map(str) + '_'+ df_analysis['Time'].map(str)
    #                        + '_' + df_analysis['Row'].str.zfill(2) + '_' + df_analysis['Col'].str.zfill(2))
    df_analysis.insert(0, 'imageID', df_analysis['Plate'].map(str) + '_'+ df_analysis['Time'].map(str)
                           + '_' + df_analysis['Well'].map(str))

    save_analysis_dataframe(df_analysis)
    return df_analysis

def save_analysis_dataframe(df_analysis):

    df_analysis.to_json(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_analysis.json'),  index=False, orient='table')
    return df_analysis

def load_analysis_dataframe( ):

    df_analysis= pd.read_json(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_analysis.json'), orient='table')

    return df_analysis


def find_abspath_files(plates=[]):

    df_analysis = load_analysis_dataframe()
    df_analysis['absPath'] =''
    # print(df_analysis)
    if not plates:
        plates = df_analysis.groupby(['Plate','Time','Folder']).size().reset_index()

    for i in range(plates.shape[0]):
        folder = os.path.join( DATA_FOLDER,  plates.at[i, 'Plate'] , plates.at[ i, 'Folder'])
        list_files = [ f for f in os.listdir(folder ) if os.path.splitext(f)[1] == DATA_ext ]

        df_subanalysis = df_analysis[ df_analysis['Plate'] == plates.at[i,'Plate'] ]
        df_subanalysis = df_subanalysis[ df_subanalysis['Time'] == plates.at[i,'Time'] ]

        for (wi, w) in zip(df_subanalysis.index.tolist(), df_subanalysis['Well'].tolist() ):
            if [os.path.join(folder, f) for f in list_files if w+'-' in f]:
                df_subanalysis.at[ wi, 'absPath'] = [os.path.join(folder, f) for f in list_files if w + '-' in f][0]
            else:
                df_subanalysis.at[wi, 'absPath'] = None

        df_analysis.update(df_subanalysis)
        df_analysis = df_analysis.convert_dtypes()
        save_analysis_dataframe(df_analysis)

    return df_analysis


def save_data_dataframe(df_analysis, name =[]):
    if not name:
        print('saved in', os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_results.json'))
        df_analysis.to_json(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_results.json'),  index=False, orient='table')
    else:
        print('saved in ',os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_'+ name +'.json'))
        df_analysis.to_json(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_'+ name +'.json'),  index=False, orient='table')
    return df_analysis


def load_data_dataframe( name =[]):
    if not name:
        df_analysis= pd.read_json(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_results.json'), orient='table')
    else:
        df_analysis = pd.read_json(  os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_'+ name +'.json'),  orient='table')
    return df_analysis


## NEW STRUCTURE 06 2021


def save_df_plates(df_plates):

    df_plates.to_feather(os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_plates.feather'))
    return df_plates


def load_df_plates():

    df_plates = pd.read_feather(os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_plates.feather'))
    return df_plates


def save_df_timepoints(df_timepoints):

    df_timepoints.to_feather(os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_timepoints.feather'))
    return df_timepoints


def load_df_timepoints():

    df_timepoints=pd.read_feather(os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_timepoints.feather'))
    return df_timepoints


def save_df_analysis(df_analysis, name =''):

    df_analysis.reset_index(drop=True, inplace=True)
    print(os.path.join(RES_FOLDER, ANALYSIS_NAME))
    filename = os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_analysis'+ name + '.json')
    df_analysis.to_json(filename, orient='split')
    return df_analysis

def load_df_analysis(name=''):

    filename = os.path.join(RES_FOLDER, ANALYSIS_NAME, ANALYSIS_NAME+'_analysis'+ name + '.json')
    print(filename)
    df_analysis=pd.read_json(filename, orient='split')
    return df_analysis


def initialization():

    #TODO: add the reading of Nx Ny Nt etc at the initialization in the different dataframe

    ## First initialize the dataframe that will stock all plates and the corresponding informations.

    list_plates = os.listdir(DATA_FOLDER)
    df_plates = pd.DataFrame(columns =['Plate', 'absPath'])
    for plate in list_plates:
        df_plates =df_plates.append({'Plate': plate, 'absPath' : os.path.join(DATA_FOLDER,plate)}, ignore_index=True)

    df_plates.sort_values(by='Plate', ascending=True, inplace=True)
    df_plates.reset_index(drop=True, inplace=True)

    df_infos = pd.read_excel(GLOBAL_INFOS)
    df_infos.dropna(thresh=5, inplace=True)
    df_plates = pd.merge(left=df_plates, right=df_infos, how='left', on=['Plate'])
    save_df_plates(df_plates)

    #TODO: implement the calculation of the Chi pulse duration and the Chi pulse start/ end
    # in terms of hours after seeding

    ## Second, initialize the timepoints dataframe that assemble all datapoints taken.

    df_timepoints = pd.DataFrame(columns=['Plate', 'Time', 'Date', 'absPath'])
    for plate in list_plates:
        folder = df_plates[ df_plates['Plate']==plate]['absPath'].iloc[0]
        list_timepoints = os.listdir(folder)
        for timepoint in list_timepoints:
            date = filu.strfind_between(timepoint, '' , '_G')
            time = filu.strfind_between(timepoint, plate+'_', 'h_')
            s = pd.Series([ plate, time, date, os.path.join(folder, timepoint)], index=df_timepoints.columns)
            df_timepoints = df_timepoints.append(s, ignore_index=True)
            
    df_timepoints.sort_values(by=['Plate', 'Date'], ascending=True, inplace=True)
    df_timepoints.reset_index(drop=True, inplace=True)
    save_df_timepoints(df_timepoints)
    project_timepoints()

    #TODO: add the time correction using the date and time infos in plate

    ## Third, initialize the dataframe for images, each row is an image

    df_analysis = pd.DataFrame(columns=[ 'Plate', 'Time', 'Well', 'Row', 'Col', 'absPath'])

    df_plates = load_df_plates()
    df_timepoints = load_df_timepoints()

    for i, plate in enumerate(df_plates['Plate']):
        df=df_timepoints[df_timepoints['Plate']==plate]

        try:
            df_ncells_plate = pd.read_excel(PLATE_NCELLS, plate).dropna(thresh=5)
            df_ncells = plate2wellist(df_ncells_plate, plate, colname='nCells')
        except:
            print('Could not find the sheet ', plate, 'in the file ', PLATE_NCELLS ,'.')
            df_ncells = pd.read_csv('../src/infos/'+ df_plates['Type'].iat[i]+'.csv', dtype=str)
            df_ncells.insert(0, 'Plate', plate)
            df_ncells.assign( nCells = None)

        for time, folder in zip( df.Time, df.absPath):
            df_well_app = df_ncells.copy()
            df_well_app.insert(1, 'Time', time)
            df_well_app.insert(2, 'absPath', None)

            list_images = [f for f in os.listdir(folder) if f.endswith(DATA_ext)]
            for (wi, w) in zip(df_well_app.index, df_well_app.Well ):
                if [os.path.join(folder, f) for f in list_images if w+'-' in f] :
                    df_well_app.at[ wi, 'absPath'] = [os.path.join(folder, f) for f in list_images if w + '-' in f][0]
            df_analysis = df_analysis.append(df_well_app)

    df_analysis.insert(0, 'imageID', df_analysis['Plate'].map(str) + '_'+ df_analysis['Time'].map(str)
                           + '_' + df_analysis['Well'].map(str))
    df_analysis.reset_index(inplace=True, drop=True)
    save_df_analysis(df_analysis)

    global INIT
    INIT = True

    return


def project_timepoints(timepoints=[24,48,72,96,120]):

    df_timepoints = load_df_timepoints()
    for i,time in enumerate(df_timepoints['Time']):
        df_timepoints.at[i, 'Time'] = timepoints[ np.argmin(np.abs([int(time)-t for t in timepoints]))]

    save_df_timepoints(df_timepoints)
    return df_timepoints


def update_wellinfos(filename, name='', colname='info'):

    df_analysis = load_analysis_dataframe()
    plates = pd.unique(df_analysis['Plate']).tolist()

    df_infos = pd.DataFrame(columns=['Plate', 'Well', 'Row', 'Col', colname])
    for plate in plates:
        try:
            df_plate = pd.read_excel(filename, plate).dropna(thresh=5)
            df_plate = plate2wellist(df_plate, plate, colname=colname)
            df_infos =df_infos.append(df_plate)
        except:
            pass

    if colname in df_analysis.columns:
        print('dropping  ', colname)
        df_analysis.drop(columns=[colname], inplace=True) # Drop the column in the _x category in the case the colname is one that you are replacing.
    df_analysis = pd.merge(df_analysis, df_infos, on=['Plate', 'Well'], how='left')
    # df_analysis.rename(columns={'Row_x': 'Row', 'Col_x': 'Col'}, inplace=True)

    for c in df_analysis.columns:
        if c.endswith('_x'):
            df_analysis.rename(columns={c: c.split('_x')[0]}, inplace=True)
    df_analysis = df_analysis[[c for c in df_analysis.columns if not c.endswith('_y')]]

    save_analysis_dataframe(df_analysis)

    return df_analysis




