
import src.image.imUtils as imu
import src.analysis.medialaxis as ma
import src.image.imUtilsJ as imuj

import numpy as np
import matplotlib.pyplot as plt

import os

import bioformats
import javabridge



def get_firstslice(filepath):

    javabridge.start_vm(class_path=bioformats.JARS)
    imu.init_logger()

    reader=bioformats.ImageReader(filepath)
    image = reader.read(rescale=False,z=0, c=0)

    # image = img[int(np.floor(img.shape[0] / 2)), :, :]

    return image

def get_secondslice(filepath):

    if filepath:

        print('filepath', filepath)

        javabridge.start_vm(class_path=bioformats.JARS)
        imu.init_logger()
        reader=bioformats.ImageReader(filepath)
        image = reader.read(rescale=False,z=1, c=0)

    return image

def get_middleslice(filepath):

    #TODO : implement this function using the metadata (number of z)
    javabridge.start_vm(class_path=bioformats.JARS)
    imu.init_logger()
    reader = bioformats.ImageReader(filepath)
    metadata= imuj.GetMetadata(filepath)
    image = reader.read(rescale=False, z=np.floor(metadata['Nz'] / 2), c=0)

    return image


def length_pipeline(filepath):

    #TODO : add fonctions names as input
    #TODO : add the debug option where the analysis is done outside of the try-except loop
    #TODO : save mask indexes = argwhere(mask) instead of the image itself!! save some memory

    from skimage.morphology import skeletonize, medial_axis

    # if z==0:
    #     image = get_firstslice(filepath)
    # elif z==1:
    #     image = get_secondslice(filepath)

    image = get_middleslice(filepath)

    Flag= 'Keep'
    length = 0

    mask=None
    try:
        lbl = imu.binarize(image)
        mask = imu.clean_binarized(lbl)
    except:
        print('Error in the binarization of image')
        Flag = 'ErrorBin'

    cnt = None
    if Flag == 'Keep':
        try:
            cnt = imu.get_contour(mask, image)
            cnt = np.asarray(cnt[0])
        except:
            print('Error in the contouring of the binary image')
            Flag = 'ErrorCnt'

    MedialAxis = None
    # if Flag == 'Keep':
    #     try:
    #         # skel = skeletonize(imuj.snake_to_bw(cnt, image.shape), method='lee')
    #         skel = medial_axis(imuj.snake_to_bw(cnt, image.shape))
    #         skel = np.argwhere(skel)
    #         MedialAxis = ma.expand_skel(skel, cnt)
    #         length = ma.get_length(MedialAxis)
    #     except:
    #         print('Error in the medial axis and length calculation')
    #         Flag = 'ErrorMedialAxis'

    volume = None
    if Flag == 'Keep':
        try:
            MedialAxis, length, volume, segments_bound, L_quarts = ma.get_volume(mask, image, ndiv=28)
        except:
            print('Error in length or volume evaluation')
            Flag = 'ErrorVolume'

    return image, Flag, mask, cnt, MedialAxis, length, volume, segments_bound, L_quarts

def length_pipeline_im(image):

    #TODO : add fonctions names as input
    #TODO : add the debug option where the analysis is done outside of the try-except loop

    from skimage.morphology import skeletonize, medial_axis

    Flag= 'Keep'
    length = 0

    mask=None
    try:
        lbl = imu.binarize(image)
        mask = imu.clean_binarized(lbl)
    except:
        print('Error in the binarization of image')
        Flag = 'ErrorBin'

    cnt = None
    if Flag == 'Keep':
        try:
            cnt = imu.get_contour(mask, image, alpha=0.2)
            cnt = np.asarray(cnt[0])
        except:
            print('Error in the contouring of the binary image')
            Flag = 'ErrorCnt'

    MedialAxis = None
    if Flag == 'Keep':
        try:
            skel = skeletonize(imuj.snake_to_bw(cnt, image.shape), method='lee')
            skel = medial_axis(imuj.snake_to_bw(cnt, image.shape))
            skel = np.argwhere(skel)
            MedialAxis = ma.expand_skel(skel, cnt)
            length = ma.get_length(MedialAxis)
        except:
            print('Error in the medial axis and length calculation')
            Flag = 'ErrorMedialAxis'

    return image, Flag, mask, cnt, MedialAxis, length

def get_contour_and_midline(I_resized):

    bw_th = imuj.im2bw_th(I_resized)
    bw_th = imu.clean_binarized(bw_th)

    from skimage.segmentation import active_contour
    cnt_init = imuj.bw_to_snake(bw_th)
    Cnt = active_contour(I_resized, cnt_init, alpha=0.15, beta=50, coordinates='rc')

    from skimage.morphology import medial_axis

    skel = medial_axis(imuj.snake_to_bw(Cnt, I_resized.shape))
    skel = np.argwhere(skel)
    Midline, ie, ib = ma.expand_skel_return(skel, Cnt)

    return Cnt, Midline

def analysis_pipeline(filepath):

    #TODO : add fonctions names as input
    #TODO : add the debug option where the analysis is done outside of the try-except loop
    #TODO : save mask indexes = argwhere(mask) instead of the image itself!! save some memory

    print(filepath)
    image = get_middleslice(filepath)


    Flag= 'Keep'

    mask=None
    mask_cnt=None
    BBOX = None
    try:
        lbl = imu.binarize(image)
        mask = imu.clean_binarized(lbl)
        mask_arg = np.argwhere(mask).T
        mask_cnt = imuj.bw_to_snake(mask)
        BBOX = [np.min( mask_arg[0])-100, np.max( mask_arg[0])+100, np.min( mask_arg[1])-100, np.max( mask_arg[1])+100]
    except:
        print('Error in the binarization of image')
        Flag = 'ErrorBin'

    cnt = None
    if Flag == 'Keep':
        try:
            cnt = imu.get_contour(mask, image)
            cnt = np.asarray(cnt[0])
        except:
            print('Error in the contouring of the binary image')
            Flag = 'ErrorCnt'

    xC,yC = cnt.T
    plt.figure()
    plt.imshow(image)
    plt.plot(yC, xC, c='k')
    plt.show()


    MedialAxis = None
    length = None
    volume = None
    segments_bound = None
    L_quarts = None

    if Flag == 'Keep':
        try:
            MedialAxis, length, segments_bound, L_quarts, volume = ma.get_volume(mask, image, ndiv=28)
        except:
            print('Error in length or volume evaluation')
            Flag = 'ErrorVolume'

    print('flag', Flag)

    return image, Flag, mask_cnt, cnt, BBOX, MedialAxis, length, segments_bound, L_quarts, volume
    # image, Flag, mask_cnt, cnt, BBOX, MedialAxis, length, segments_bound,volume, volumes, lengths
    # return 0

def morphological_analysis_file(filepath, ndiv=50):

    image = get_middleslice(filepath)

    #TODO : add fonctions names as input
    #TODO : add the debug option where the analysis is done outside of the try-except loops
    #TODO : make the analysis given a filepath load the image and then apply the analysis given an image,
    # parameters: the way to read the image given the adress (first slice, middle slice, max proj ?)
    #TODO: implement the exception exception as e , print(e) to see what are the errors that I get in the command when it doesnot work properly !!!

    Flag= 'Keep'

    mask_cnt=None
    BBOX = None
    try:
        lbl = imu.binarize(image)
        mask = imu.clean_binarized(lbl)
        mask_arg = np.argwhere(mask).T
        mask_cnt = imuj.bw_to_snake(mask)
        BBOX = [np.min(mask_arg[0]) - 100, np.max(mask_arg[0]) + 100, np.min(mask_arg[1]) - 100,
                np.max(mask_arg[1]) + 100]

    except:
        print('Error in the binarization of image')
        Flag = 'ErrorBin'

    cnt = None
    if Flag == 'Keep':
        try:
            cnt = imu.get_contour(mask, image, alpha=0.05, beta=1, w_line=0, w_edge=1)
            cnt = np.asarray(cnt[0])
        except:
            print('Error in the contouring of the binary image')
            Flag = 'ErrorCnt'

    MedialAxis = None
    length = None
    if Flag == 'Keep':
        try:
            MedialAxis, length = ma.get_axis_and_length( cnt, image)
        except:
            print('Error in medial axis and length evaluation')
            Flag = 'ErrorMedax'

    segments_bound = None
    volume = None
    volumes = None
    lengths = None
    thicknesses = None

    if Flag == 'Keep':
        try:
            nbin=50
            volume, segments_bound, volumes, lengths, thicknesses = ma.bin_volume(MedialAxis, cnt, image, ndiv=nbin)
        except:
            print('Error in volume evaluation')
            Flag = 'ErrorVolume'

    print(type(volume))

    print("Flag", Flag)
    return image, Flag, mask_cnt, cnt, BBOX, MedialAxis, length, segments_bound, volume, volumes, lengths, thicknesses



def morphological_analysis_image(image, um_per_pixel, ndiv=50):

    import cv2
    coef = 0.5/um_per_pixel
    dim = tuple( [np.int(np.round(x/coef)) for x in image.shape])
    image_resized = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)
    image=image_resized
    #TODO : add fonctions names as input
    #TODO : add the debug option where the analysis is done outside of the try-except loop
    #TODO : save mask indexes = argwhere(mask) instead of the image itself!! save some memory
    #TODO : add the BKGD background value outside of mask

    Flag= 'Keep'

    mask_cnt=None
    BBOX = None
    try:
        lbl = imu.binarize(image)
        mask = imu.clean_binarized(lbl)
        mask_arg = np.argwhere(mask).T
        mask_cnt = imuj.bw_to_snake(mask)
        BBOX = [np.min(mask_arg[0]) - 100, np.max(mask_arg[0]) + 100, np.min(mask_arg[1]) - 100,
                np.max(mask_arg[1]) + 100]

    except:
        print('Error in the binarization of image')
        Flag = 'ErrorBin'

    cnt = None
    if Flag == 'Keep':
        try:
            cnt = imu.get_contour(mask, image, alpha=0.05, beta=0.5, w_line=0, w_edge=0)
            cnt = np.asarray(cnt[0])
        except:
            print('Error in the contouring of the binary image')
            Flag = 'ErrorCnt'

    MedialAxis = None
    length = None
    if Flag == 'Keep':
        try:
            MedialAxis, length = ma.get_axis_and_length( cnt, image)
        except:
            print('Error in length evaluation')
            Flag = 'ErrorLength'

    segments_bound = None
    volume = None
    volumes = None
    lengths = None

    if Flag == 'Keep':
        try:
            volume, segments_bound, volumes, lengths, thicknesses = ma.bin_volume(MedialAxis, cnt, image, ndiv=ndiv)
        except:
            print('Error in volume evaluation')
            Flag = 'ErrorVolume'

    if Flag == 'Keep':
        mask_cnt = mask_cnt*coef
        cnt = cnt*coef
        BBOX= [x*coef for x in BBOX]
        MedialAxis = coef*MedialAxis
        length = length*coef
        segments_bound= [np.array(segments_bound)*coef]
        volume= volume*coef**3
        volumes= [v *coef**3 for v in volumes]
        lengths= [l*coef for l in lengths]


    return coef, Flag, mask_cnt, cnt, BBOX, MedialAxis, length, segments_bound,volume, volumes, lengths


def profiles_analysis(serie_IF):

    #TODO: calculate xbin and xproj and add in in return

    import networkx as nx
    from sklearn.neighbors import kneighbors_graph, radius_neighbors_graph
    import src.analysis.medialaxis as ma

    imagefile = serie_IF['absPath']
    dict_ch = serie_IF['channels']
    nchannels = len(dict_ch)
    if os.path.splitext(imagefile)[1] == '.tif':
        import tifffile
        print('tif!')
        image = tifffile.imread(imagefile)
    else:
        print('not tif!')
        image = get_middleslice(imagefile)
        if nchannels==1:
            image_new = []
            image_new.append(image)
            image = np.array(image_new)
    print('shape', image.shape)
    if image.shape[0] != nchannels:
        print('number of specified channels is incorrect')
        pass


    ndiv = len(serie_IF['Lengths'])
    print('ndiv', ndiv)
    segments = np.array(serie_IF['Segments']).squeeze()
    print( 'len segments' , len(segments))
    subX1 = segments[:,0,0]
    subX2 = segments[:,1,0]
    subY1 = segments[:,0,1]
    subY2 = segments[:,1,1]

    cnt = np.array(serie_IF['Contour'])
    xCA,yCA = cnt.T

    Gcnt = kneighbors_graph(cnt, 2, mode='distance')
    Tcnt = nx.from_scipy_sparse_matrix(Gcnt)
    end_cliques = [i for i in list(nx.find_cliques(Tcnt)) if len(i) == 3]
    edge_lengths = [ma.find_longest_edge(i, Tcnt) for i in end_cliques]
    Tcnt.remove_edges_from(edge_lengths)

    A=[]
    B=[]
    for i in range(len(subX1)):
        A.append(np.where((xCA == subX1[i]) & (yCA == subY1[i]))[0][0])
        B.append(np.where((xCA == subX2[i]) & (yCA == subY2[i]))[0][0])

    img_dim =  image[0,:,:].shape
    imgtot = np.zeros(img_dim)

    f = []
    for i in range(ndiv):

        xxx = [subX1[i], subX2[i]]
        yyy = [subY1[i], subY2[i]]

        dx = np.diff(xxx)

        if not dx == 0:
            m = (yyy[1] - yyy[0]) / (xxx[1] - xxx[0])
            p = yyy[0] - m * xxx[0]
            f.append(np.poly1d([m, p]))
        else:
            f.append([])


    for ii in range(ndiv):

        # print(ii)

        Icnt1 = nx.shortest_path(Tcnt, source=A[ii], target=A[ii + 1])
        Icnt2 = nx.shortest_path(Tcnt, source=B[ii], target=B[ii + 1])
        xpnt = np.concatenate((xCA[Icnt1], xCA[Icnt2]), axis=None)
        ypnt = np.concatenate((yCA[Icnt1], yCA[Icnt2]), axis=None)

        dx = np.abs( subX2[ii] - subX1[ii])
        dy = np.abs( subY2[ii] - subY1[ii])
        if dx > 10**-2:
            xx = np.linspace(subX1[ii], subX2[ii], 2*np.int( dx*np.max([1,np.abs(f[ii][1])])))
            yy = f[ii](xx)
        else:
            if dy < 10**-2:
                xx = []
                yy = []
            else:
                yy = np.linspace(subY1[ii], subY2[ii], 2*np.int(dy))
                xx = subX1[ii] * np.ones(np.shape(yy))

        dxx = np.abs(subX2[ii+1] - subX1[ii+1])
        dyy = np.abs(subY2[ii+1] - subY1[ii+1])
        if dxx > 10**-2:
            xx2 = np.linspace(subX1[ii + 1], subX2[ii + 1], 2*np.int( dxx*np.max([1,np.abs(f[ii+1][1])])))
            yy2 = f[ii+1](xx2)
        else:
            if dyy <10**-2:
                xx2 = []
                yy2 = []
            else:
                yy2 = np.linspace(subY1[ii], subY2[ii], 10*np.int(dyy))
                xx2 = subX1[ii+1] * np.ones(np.shape(yy2))

        if ii == 0:
            xpnt = np.concatenate((xpnt, xx2), axis=None)
            ypnt = np.concatenate((ypnt, yy2), axis=None)
        if ii == ndiv - 2:
            xpnt = np.concatenate((xpnt, xx), axis=None)
            ypnt = np.concatenate((ypnt, yy), axis=None)
        else:
            xpnt = np.concatenate((xpnt, xx, xx2), axis=None)
            ypnt = np.concatenate((ypnt, yy, yy2), axis=None)

        sna = np.vstack((xpnt, ypnt)).T
        imgtest = imuj.snake_to_bw(sna, img_dim)
        ind = np.argwhere(imgtest).T
        imgtot[ind[0], ind[1]] = ii + 1

    area = np.zeros(ndiv)
    for i in range(ndiv):
        area[i] = np.argwhere(imgtot == i + 1).shape[0]

    serie_IF['Areas'] = area

    for ch in dict_ch:
        G = np.zeros(ndiv)
        img=image[dict_ch[ch],:,:]
        for i in range(ndiv):
            G[i] = np.sum(img[np.nonzero(imgtot == i+1)])
        serie_IF[ch] = G

    # print(serie_IF)

    return serie_IF

def profiles_analysis_curves(serie_IF):

    #TODO: save the coordinates of the curved binning segments

    import networkx as nx
    from sklearn.neighbors import kneighbors_graph, radius_neighbors_graph
    import src.analysis.medialaxis as ma
    from src.analysis.medialaxis import find_longest_edge, path_sublength, path_cost

    imagefile = serie_IF['absPath']
    print(imagefile)
    dict_ch = serie_IF['channels']
    nchannels = len(dict_ch)
    if os.path.splitext(imagefile)[1] == '.tif':
        import tifffile
        image = tifffile.imread(imagefile)
    else:
        print('not tif!')
        image = get_middleslice(imagefile)
        if nchannels == 1:
            image_new = []
            image_new.append(image)
            image = np.array(image_new)
    print('shape', image.shape)
    if image.shape[0] != nchannels:
        print('number of specified channels is incorrect')
        pass


    ndiv = 200

    med = np.array(serie_IF['MedialAxis'])
    xMed, yMed = med.T

    Gmed = kneighbors_graph(med, 2, mode='distance')
    Tmed = nx.from_scipy_sparse_matrix(Gmed)
    end_cliques = [i for i in list(nx.find_cliques(Tmed)) if len(i) == 3]
    edge_lengths = [find_longest_edge(i, Tmed) for i in end_cliques]
    Tmed.remove_edges_from(edge_lengths)

    if len([ (n,d) for (n,d) in Tmed.degree() if d >2]) > 1:
        serie_IF['Flag'] = 'BranchedMedax'
        return serie_IF

    ## Path along the midline
    L = []
    P = []
    for path in nx.all_simple_paths(Tmed, source=0, target=len(Tmed) - 1):
        P.append(path)
        L.append(path_cost(Tmed, path))
    print(L)

    ## Subdivide each side
    for i, l in enumerate(L):

        subl = 0
        I = [P[i][0]]
        for j in range(ndiv - 1):
            subl = subl + l / ndiv
            I.append(path_sublength(Tmed, P[i], subl))
        I.append(P[i][-1])

        if i == 0:
            A = I
            subX1 = [xMed[k] for k in I]
            subY1 = [yMed[k] for k in I]
    fusionP = np.int(np.round(ndiv / 50))  # (= 2%)
    print('removing each ', fusionP, ' first segments at each tip')

    INDEX_Seg = range(fusionP, len(subX1) - fusionP)

    M = []
    for i in INDEX_Seg:
        xxx = subX1[i]
        yyy = subY1[i]
        M.append([xxx, yyy])

    segments = np.array(serie_IF['Segments']).squeeze()
    S=segments
    S1 = np.squeeze(S[1:-1, 0, :])
    S2 = np.squeeze(S[1:-1, 1, :])
    subX1 = segments[:, 0, 0]
    subX2 = segments[:, 1, 0]
    subY1 = segments[:, 0, 1]
    subY2 = segments[:, 1, 1]

    cnt = np.array(serie_IF['Contour'])
    xCA, yCA = cnt.T
    Gcnt = kneighbors_graph(cnt, 2, mode='distance')
    Tcnt = nx.from_scipy_sparse_matrix(Gcnt)
    end_cliques = [i for i in list(nx.find_cliques(Tcnt)) if len(i) == 3]
    edge_lengths = [ma.find_longest_edge(i, Tcnt) for i in end_cliques]
    Tcnt.remove_edges_from(edge_lengths)

    A = []
    B = []
    for i in range(len(subX1)):
        A.append(np.where((xCA == subX1[i]) & (yCA == subY1[i]))[0][0])
        B.append(np.where((xCA == subX2[i]) & (yCA == subY2[i]))[0][0])

    XX = [S[0, 1, :]]
    YY = [S[0, 0, :]]

    rot = lambda X, Y, a: [[x * np.cos(a) + y * np.sin(a), -x * np.sin(a) + y * np.cos(a)] for (x, y) in zip(X, Y)]
    roti = lambda X, Y, a: [[x * np.cos(a) - y * np.sin(a), x * np.sin(a) + y * np.cos(a)] for (x, y) in zip(X, Y)]

    angles=[]
    for i, (s1, s2, m) in enumerate(zip(S1, S2, M)):
        x = [s1[1], s2[1], m[1]]
        y = [s1[0], s2[0], m[0]]
        a = np.arctan(np.abs((y[1] - y[0]) / (x[1] - x[0]))) * np.sign((y[1] - y[0]) / (x[1] - x[0]))
        angles.append(a)
        R = rot(x, y, a)
        x = [R[0][0], R[1][0], R[2][0]]
        y = [R[0][1], R[1][1], R[2][1]]
        z = np.polyfit(x, y, 2)
        p = np.poly1d(z)

        X = np.linspace(x[0], x[1], int(np.abs(x[1] - x[0]) + 10))
        Y = p(X)
        R = roti(X, Y, a)
        xx = []
        yy = []
        for j in range(len(R)):
            xx.append(R[j][0])
            yy.append(R[j][1])
        XX.append(yy)
        YY.append(xx)
    XX.append(S[-1, 1, :])
    YY.append(S[-1, 0, :])

    img_dim = image[0, :, :].shape
    imgtot = np.zeros(img_dim)

    ndiv = len(serie_IF.Lengths)
    for ii in range(ndiv):

        Icnt1 = nx.shortest_path(Tcnt, source=A[ii], target=A[ii + 1])
        Icnt2 = nx.shortest_path(Tcnt, source=B[ii], target=B[ii + 1])
        xpnt = np.concatenate((xCA[Icnt1], xCA[Icnt2]), axis=None)
        ypnt = np.concatenate((yCA[Icnt1], yCA[Icnt2]), axis=None)

        xx = XX[ii]
        yy = YY[ii]

        xx2 = XX[ii + 1]
        yy2 = YY[ii + 1]

        if ii == 0:
            xpnt = np.concatenate((xpnt, xx2), axis=None)
            ypnt = np.concatenate((ypnt, yy2), axis=None)
        if ii == ndiv - 1:
            xpnt = np.concatenate((xpnt, xx), axis=None)
            ypnt = np.concatenate((ypnt, yy), axis=None)
        else:
            xpnt = np.concatenate((xpnt, xx, xx2), axis=None)
            ypnt = np.concatenate((ypnt, yy, yy2), axis=None)

        sna = np.vstack((xpnt, ypnt)).T
        imgtest = imuj.snake_to_bw(sna, img_dim)
        ind = np.argwhere(imgtest).T
        imgtot[ind[0], ind[1]] = ii + 20

    area = np.zeros(ndiv)
    for i in range(ndiv):
        area[i] = np.argwhere(imgtot == i + 21).shape[0]

    serie_IF['Areas'] = area
    serie_IF['Angles'] = angles

    for ch in dict_ch:
        G = np.zeros(ndiv)
        img = image[dict_ch[ch], :, :]
        for i in range(ndiv):
            G[i] = np.sum(img[np.nonzero(imgtot == i + 21)])
        serie_IF[ch] = G

    return serie_IF





