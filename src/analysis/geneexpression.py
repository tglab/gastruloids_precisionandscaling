import numpy as np

import src.image.imUtils as imu
import src.analysis.medialaxis as medax
import networkx as nx

def subdivide_curve(G_curve, ndiv, metod ='raw', curve_C = None):
    '''
    A function that sudivide a given curve, stored as a networkx object,
    into equirepartited curvilinear segments

    arguments:
    G_curve : graph describing the curve (networkx object), need to be ordered
    nbin : desired number of sub-segments
    curve_C : ndarray of size (ndim, npts), coordinates of the curve known points

    returns:
    I : ndiv+1 list , indexes of the subdivisions boundaries points belonging to G_cruve
    sub_C : coordinates of points along the curve
    '''

    #TODO : how to now where is the start and the beginning of the path ? reorder here or before ?

    path = list(nx.shortest_path(G_curve, source=0, target=len(G_curve)-1))
    Length = medax.path_cost(G_curve, path)
    I = [path[0]]
    subl = 0
    for j in range(ndiv - 1):
        subl = subl + Length / ndiv
        I.append(medax.path_sublength(G_curve, path, subl))
    I.append(path[-1])

    if curve_C is None:
        points = None
    else:
        points = np.empty( (curve_C.shape[0], ndiv+1))
        for i in range(curve_C.shape[0]):
            points[i]=curve_C[i][I]

    return I, points


def bin_gastruloid( contour_C, midline_C, ndiv, method='cnt_division'):
    ''' A function to bin a gastruloid along a given axis '''

    print('bining, test')

    midline_C, T_ML = medax.orderPoints(midline_C)
    contour_C, T_CNT = medax.orderPoints(contour_C)

    print(midline_C.shape)
    print(contour_C.shape)

    print(contour_C[:,0])
    print(midline_C[0,0])
    # print( np.intersect1d( np.intersect1d (contour_C[:,0], midline_C[0,0]),  np.intersect1d (contour_C[:,1], midline_C[1,0])))
    #
    # ndiv = 50
    #
    # for i, l in enumerate(L):
    #     subl = 0
    #     I = [P[i][0]]
    #     for j in range(ndiv - 1):
    #         subl = subl + l / ndiv
    #         I.append(medax.path_sublength(Tcnt, P[i], subl))
    #     I.append(P[i][-1])
    #
    #     if i == 0:
    #         A = I
    #         suddivpointsX_1 = [xCA[k] for k in I]
    #         suddivpointsY_1 = [yCA[k] for k in I]
    #     if i == 1:
    #         B = I
    #         suddivpointsX_2 = [xCA[k] for k in I]
    #         suddivpointsY_2 = [yCA[k] for k in I]
    #
    # #         subl = 0
    #
    # path = list(nx.shortest_path(Tma, source=0, target=len(xMA) - 1))
    # Length = medax.path_cost(Tma, path)
    # I = [path[0]]
    # subl = 0
    # for j in range(ndiv - 1):
    #     subl = subl + Length / ndiv
    #     I.append(medax.path_sublength(Tma, path, subl))
    # I.append(path[-1])
    # suddivpointsX_ma = [xMA[k] for k in I]
    # suddivpointsY_ma = [yMA[k] for k in I]
    #
    # imgtot = np.zeros(I_resized.shape)
    #
    # for ii in range(0, ndiv):
    #     # for ii in range(0,1):
    #
    #     # Ima = nx.shortest_path(Tma, source = M[ii], target= M[ii+1])
    #     Icnt1 = nx.shortest_path(Tcnt, source=A[ii], target=A[ii + 1])
    #     Icnt2 = nx.shortest_path(Tcnt, source=B[ii], target=B[ii + 1])
    #     xpnt = np.concatenate((xCA[Icnt1], xCA[Icnt2]), axis=None)
    #     ypnt = np.concatenate((yCA[Icnt1], yCA[Icnt2]), axis=None)
    #     if np.sign(suddivpointsX_2[ii] - suddivpointsX_1[ii]) == 0:
    #         xx = []
    #     else:
    #         xx = np.arange(suddivpointsX_1[ii], suddivpointsX_2[ii],
    #                        0.01 * np.sign(suddivpointsX_2[ii] - suddivpointsX_1[ii]))
    #     if np.sign(suddivpointsX_2[ii + 1] - suddivpointsX_1[ii + 1]) == 0:
    #         xx2 = []
    #     else:
    #         xx2 = np.arange(suddivpointsX_1[ii + 1], suddivpointsX_2[ii + 1],
    #                         0.01 * np.sign(suddivpointsX_2[ii + 1] - suddivpointsX_1[ii + 1]))
    #
    #     if ii == 0:
    #         xpnt = np.concatenate((xpnt, xx2), axis=None)
    #         ypnt = np.concatenate((ypnt, f[ii](xx2)), axis=None)
    #     if ii == ndiv - 1:
    #         xpnt = np.concatenate((xpnt, xx), axis=None)
    #         ypnt = np.concatenate((ypnt, f[ii - 1](xx)), axis=None)
    #     else:
    #         xpnt = np.concatenate((xpnt, xx, xx2), axis=None)
    #         ypnt = np.concatenate((ypnt, f[ii - 1](xx), f[ii](xx2)), axis=None)
    #
    #     sna = np.vstack((xpnt, ypnt)).T
    #     imgtest = imuj.snake_to_bw(sna, I_resized.shape)
    #     ind = np.argwhere(imgtest).T
    #     imgtot[ind[0], ind[1]] = ii + 1
    #
