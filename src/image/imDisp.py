import matplotlib.pyplot as plt
import numpy as np


def dispIm(ax,img, cmap='gray'):

    ''' display an image given
    ax: the axis  object
    img : the image to show
    cmap : the colormap to use

    return the modified axis object
    '''

    ax.imshow(img, cmap=cmap)
    ax.set_axis_off()
    ax.axis('scaled')
    ax.set_frame_on(True)

    return ax


def addContour(ax, cnt, color='r', **kwargs):

    '''add a contour given the coordinates:
    ax : the axis object
    cnt : the coordinates of the contour as an array object
    color: in which color to draw the contour
    **kwargs : supplementary parameters to the plot function'''

    if not isinstance(cnt, np.ndarray):
        cnt = np.asarray(cnt)
    if cnt.shape[1] !=2:
        print('Wrong dimensions of the contour array : ', cnt.shape, '. It should be npts x 2.')
        return ax
    xCnt, yCnt = cnt.T
    ax.plot(yCnt, xCnt, c=color, **kwargs)