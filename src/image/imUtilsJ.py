import numpy as np
import matplotlib.pyplot as plt


#########################################################
################### Reading Functions ###################
#########################################################

def ListFields(object):
    '''
    from an object, we can get all the attributes/methods by using the dir() function
    '''

    object_attributes = [method_name for method_name in dir(object)
                         if not callable(getattr(object, method_name))]
    object_methods = [method_name for method_name in dir(object)
                      if callable(getattr(object, method_name))]
    object_all = [method_name for method_name in dir(object)]

    return object_all,object_attributes,object_methods



def GetMetadata(filename):
    '''
    from a czi, I want to get multiple informations :
    -number of pixels
    -physical pixel size
    '''
    import bioformats

    md              = bioformats.get_omexml_metadata( filename )
    metadata = {
        #         "fullmetadata" : md,
        "Nx"        : bioformats.OMEXML(md).image().Pixels.SizeX,
        "Ny"        : bioformats.OMEXML(md).image().Pixels.SizeY,
        "Nz"        : bioformats.OMEXML(md).image().Pixels.SizeZ,
        "Nt"        : bioformats.OMEXML(md).image().Pixels.SizeT,
        "Nch"       : bioformats.OMEXML(md).image().Pixels.SizeC,
        "dtype_str" : bioformats.OMEXML(md).image().Pixels.PixelType,
        "dx"        : bioformats.OMEXML(md).image().Pixels.PhysicalSizeX,
        "dxUnit"    : bioformats.OMEXML(md).image().Pixels.PhysicalSizeXUnit,
        "dy"        : bioformats.OMEXML(md).image().Pixels.PhysicalSizeY,
        "dyUnit"    : bioformats.OMEXML(md).image().Pixels.PhysicalSizeYUnit,
        "dz"        : bioformats.OMEXML(md).image().Pixels.PhysicalSizeZ,
        "dzUnit"    : bioformats.OMEXML(md).image().Pixels.PhysicalSizeZUnit,
        "Nseries"   : bioformats.ImageReader(filename).rdr.getSeriesCount()
    }

    return metadata



#########################################################
################### Other Functions ###################
#########################################################



def smallObjectPlane(stack,Nz):
    ''' This function calculates the standard deviation intensity for each image across the stack
    It returns the index of the plane that has the most variation (the more defined)'''
    from skimage.filters import threshold_otsu
    from skimage.filters import gaussian
    bwsum=np.zeros((Nz,1))
    for i in range(0,Nz):
        im=stack[:,:,i]-stack.mean(axis=2)
        im_filtered=gaussian(im,sigma=5)
        th=threshold_otsu(im_filtered)
        bw=im_filtered>th
        bwsum[i]=bw.sum()
    return np.argmin(bwsum)



#########################################################
################### Object Detection Functions ###################
#########################################################



def im2bw_th(img,f=1.0):
    '''
    This function gives good results if the objects to be detected have internal intensity fluctuations.
    small objects are removed.
    Inputs:
    img is the image
    f is a factor that is applied during thresholding (optional, default value=1.0)
    Output:
    bw2, binary image
    '''
    from skimage import filters
    from skimage.morphology import disk
    from skimage.morphology import dilation
    from skimage.morphology import erosion
    from skimage.morphology import disk
    from skimage.morphology import remove_small_objects
    from scipy import ndimage as ndi
    im_filtered=(img-img.min())/(img.max()-img.min())
    edges_sobel=filters.sobel(im_filtered)
    edges_sobel=filters.gaussian(edges_sobel,2)
    th=filters.threshold_otsu(edges_sobel)
    bw=edges_sobel>th*f
    selem = disk(10)
#     bw2=remove_small_objects(bw)
    bw2=bw
    bw2=dilation(bw2,selem=selem)
    bw2=ndi.binary_fill_holes(bw2)
    bw2=erosion(bw2,selem=selem)
    bw2=remove_small_objects(bw2,min_size=200)
    return bw2


def bw_to_snake(bw):
    '''
    Returns the snake (contour only coordinates) from a filled binary image
    Did not test if it supports holes.
    :param bw: binary image
    :return: snake 2D array with the positions of the contour of the binary image
    '''

    from skimage import measure
    snake=np.array(measure.find_contours(bw,0.5))
    snake=snake.reshape((snake.shape[1],snake.shape[2]))

    return snake

def snake_to_bw(snake,shape):
    '''
    Very cruedly rounds the positions in listed in the snake to pixel positions to produce a filled binary image.
    :param snake: 2D array of positions
    :param shape: shape of the binary image desired
    :return: bw_snake
    '''
    from scipy.ndimage import binary_fill_holes
    import cv2
    bw_snake= np.zeros(shape)
    snake=np.int0(snake)
    for i in range(np.size(snake,0)):
        cv2.circle(bw_snake,(snake[i,1], snake[i,0]), radius=2, color=(255,255,255), thickness=-1)
        # bw_snake[positions[:, 0], positions[:, 1]] = 1
    bw_snake = binary_fill_holes(bw_snake)

    return bw_snake


def snake_to_cnt(snake,shape):
    '''
    Very cruedly rounds the positions in listed in the snake to pixel positions to produce a filled binary image.
    :param snake: 2D array of positions
    :param shape: shape of the binary image desired
    :return: bw_snake
    '''
    from scipy.ndimage import binary_fill_holes
    import cv2
    bw_snake= np.zeros(shape)
    snake=np.int0(snake)
    for i in range(np.size(snake,0)):
        cv2.circle(bw_snake,(snake[i,1], snake[i,0]), radius=2, color=(255,255,255), thickness=-1)
        # bw_snake[positions[:, 0], positions[:, 1]] = 1
    # bw_snake = binary_fill_holes(bw_snake)

    return bw_snake


def find_downscaled_cv(img,ds,contour,iterations,sm=3):
    '''
    Function downscales image using binning factor ds, then uses previous contour to find new contour.
    Inputs:
    img is the original image
    ds is the binning factor
    contour is the initial contour for this function (a binary image)
    iterations is max number of iterations for the active contour algorithm
    output:
    new contour
    '''
    from skimage.transform import downscale_local_mean,resize
    from skimage.segmentation import morphological_chan_vese
    img_ds=downscale_local_mean(img, (ds, ds))
    init_contour_ds=resize(contour,img_ds.shape)
    init_contour_ds=init_contour_ds>init_contour_ds.min()
    contour_ds = morphological_chan_vese(img_ds, iterations, init_level_set=init_contour_ds, smoothing=sm)
    return contour_ds

def im2bw_cv(img,sm=3):
    '''
    Function generates binary image using active contour usint Chan-vese algo.
    Inputs:
    img
    Ouput:
    contour, binary image
    '''
    from skimage.filters import threshold_otsu, sobel
    img=(img-img.min())/(img.max()-img.min())
    edges_sobel=sobel(img)
    edges_sobel=(edges_sobel-edges_sobel.min())/(edges_sobel.max()-edges_sobel.min())
    init_contour = np.zeros(edges_sobel.shape, dtype=np.int8)
    init_contour[10:-10, 10:-10] = 1
    contour=init_contour
    iterations=100

    for n in range(5,-1,-1):
        contour=find_downscaled_cv(edges_sobel,2**n,contour,iterations,sm)
        if contour.sum()==0:
            contour=init_contour
        else:
            iterations=10
    return contour


def Visualize(img,title):
    '''
    Just to create visualisation
    '''
    from skimage import exposure

    fig, axes = plt.subplots(1, 1, figsize=(5, 5))

    p2, p98 = np.percentile(img, (2, 98))
    img_rescale = exposure.rescale_intensity(img, in_range=(p2, p98))
    axes.imshow(img_rescale, cmap="gray")
    axes.set_axis_off()
    axes.set_title(title, fontsize=12)
    fig.tight_layout()
    # plt.show()
    return fig , axes

def VisualizeContour(img,bw,title):
    '''
    Just to create visualisation
    '''
    from skimage import exposure

    fig, axes = plt.subplots(1, 1, figsize=(5, 5))

    p2, p98 = np.percentile(img, (2, 98))
    img_rescale = exposure.rescale_intensity(img, in_range=(p2, p98))
    axes.imshow(img_rescale, cmap="gray")
    axes.contour(bw,[0.5],colors='r')
    axes.set_axis_off()
    axes.set_title(title, fontsfize=12)
    fig.tight_layout()
    # plt.show()
    return fig , axes

def VisualizeContourSkeleton(img,bw,sk,title):
    '''
    Just to create visualisation
    '''
    from skimage import exposure

    fig, axes = plt.subplots(1, 1, figsize=(5, 5))

    p2, p98 = np.percentile(img, (2, 98))
    img_rescale = exposure.rescale_intensity(img, in_range=(p2, p98))
    axes.imshow(img_rescale, cmap="gray")
    axes.contour(bw,[0.5],colors='r')
    axes.contour(sk,[0.5],colors='b')
    axes.set_axis_off()
    axes.set_title(title, fontsize=12)
    fig.tight_layout()
    # plt.show()
    return fig , axes

def AddContour(ax,bw,color,lw):
    '''
    Adds a contour based on a binary image to a given ax
    :param ax: ax object to use
    :param bw : binary image
    :param color : color of the contour
    :param lw : width of the contour
    :return: none
    '''
    ax.imshow(np.zeros((1,1)),alpha=0)
    ax.contour(bw,[0.5],colors=color,linewidths=lw)


def AddSnake(ax,snake,color,lw_,imshape=(1,1)):
    '''
    Adds a contour based on a snake (coordinates of the contour)  to a given ax
    imshape is needed if one wants the raw plot to be shaped as the initial image
    :param ax: ax object to use
    :param snake : coordinates of the contour
    :param color : color of the contour
    :param lw_ : width of the contour
    :param imshape : size of the original image
    :return: none
    '''
    ax.imshow(np.zeros(imshape),alpha=0)
    ax.plot(snake[:,1],snake[:,0],[0.5],c=color,lw=lw_)

def AddScale(ax,scale_pix,scale_length,Verticalsize,s=12,unit='$\mu$m',c='white',ratio=50):
    '''
    Adds a scale bar to ax object
    :param ax: ax object to use
    :param scale_pix: size of one pixel in unit
    :param scale_length: length of the scalebar in unit
    :param Verticalsize: number of rows in pixel of the image
    :param s: size of the text
    :param unit: unit in which the size is expressed (unit is micron by default)
    :param c: color of the scalebar and text (default is white)
    :param ratio: 
    :return:

    example :

    ImUtils.AddScale(axes[3],scale_pix,scale_length,I.shape[1],c='k')

    '''
    from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar
    import matplotlib.font_manager as fm
    fontprops = fm.FontProperties(size=s)
    scalebar = AnchoredSizeBar(ax.transData,
                               scale_pix, str(scale_length) + unit, 'lower right',
                               pad=1,
                               color=c,
                               frameon=False,
                               size_vertical=Verticalsize/ ratio,
                               fontproperties=fontprops)

    ax.add_artist(scalebar)