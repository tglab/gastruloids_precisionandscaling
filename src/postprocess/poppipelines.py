#This file contain pipelines of analysis post image analysis to describe a list of actions to apply on the data

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sbs
import os

from src.postprocess import APprofiles as ap
from src.postprocess import popgeneral as popg
from src.postprocess import statistics as stat
import src.structure.filUtils as filu


def initialize(main_folder, name_analysis, file_name):

    if not os.path.exists(main_folder):
        os.mkdir(main_folder)
    results_folder = os.path.join(main_folder, name_analysis)
    if not os.path.exists(results_folder):
        os.mkdir(results_folder)

    data_file = os.path.join(main_folder, file_name +'.json')
    import time
    tstart = time.time()
    df = pd.read_json(data_file, orient='split')
    tend = time.time()
    print('Loaded the analysis dataframe in ', (tend - tstart) / 60, 'min')

    def figsave(fig, name):
        filu.figsave(results_folder, fig, name_analysis + '_' + name)

    return df, results_folder, figsave


def pipeline_tot(df):
    '''  '''

    return df_tot

def clean_dataframe(df, showhist=False):
    ''' Function that removes all the row for which the analysis has not been pursued to the end, using the Flag values
    df: dataframe to process
    showhist: if True, display the histogram of Flags
    '''

    df_keep = df[df['Flag'] == 'Keep']
    print( str(100-len(df_keep)/len(df)*100) + '% (' +  str(len(df)- len(df_keep)) + '/' + str(len(df)) +') of images removed because the analysis was not properly completed. \n')

    fig_hist = plt.figure(figsize=(5,5))
    sbs.histplot(data=df, x='Flag', color='b', shrink=0.9)
    plt.show()

    fig_montage, _ = popg.makeMontageAssay(df_keep, channels=[0,1,2], colors=[1,2,0],ratio=0.5, explore=True, figheight= 15,
                                  ROT=False, MASK=False, CNT=True, MEDAX=True, BBOX=False)
    plt.show()

    noutliers = int(input('How much outliers do you see ? '))
    outliers=[]
    print('Which pictures to remove? Enter the full imageID as written in the figure.')
    for i in range(noutliers):
        outliers.append(str(input()))

    print( '\nOutliers:')
    print(outliers)
    while len(np.intersect1d(df_keep.imageID.values, outliers)) != len(outliers):
        print('Outliers contain values that are not appropriate imageID!')
        for k in np.setdiff1d( outliers, df_keep.imageID.values):
            print('Please correct the ' + str(outliers.index(k)) + 'th value: ' + k)
            outliers[outliers.index(k)] = str(input())

    df_clean = df_keep[~df_keep['imageID'].isin(outliers)]
    print( '\n' + str(100-len(df_clean)/len(df_keep)*100) + '% (' +  str(len(df_keep)- len(df_clean)) + '/' + str(len(df_keep)) +') of remaining images were visually identified as outliers')

    df_eluded = df[~df['imageID'].isin(df_clean.imageID.values)]
    print( 'Finally, ' +  str(len(df_clean)/len(df)*100) + '% (' +  str(len(df_clean)) + '/' + str(len(df)) +') of analysis were kept.')

    if showhist:
        return df, df_clean, df_eluded, fig_montage, fig_hist
    else:
        return df, df_clean, df_eluded, fig_montage



def find_outliers(df, **kwargs):
    ''' Plot the analyzed images to see which ones to discard '''

