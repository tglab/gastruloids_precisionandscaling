import numpy as np


def getdistribution(df, keys, group_keys):
    ''' Function that gives the distributions of the values of keys grouping the data by group_keys
    df: dataframe to analyse
    keys: keys for which to plot the distribution
    group_keys: keys by which to group the data in df '''

    #TODO: implement the function that gives distributions for a given key
    #TODO: think of how to separate the different case: unique values stat vs list along the AP




    return fig

def getIDimage(df, keyx, keyy, keyid, **kwargs):
    ''' Return an imshow of the matrix of profiles of keyy along the axis keyx, and annotate using ID'''

    #TODO: implement and test the function

    xx = []
    yy = []
    for i in df.index:
        xx.append(df.at[i,keyx])
        yy.append(df.at[i,keyy])
        if len(df.at[i, keyy]) != np.shape(yy)[1]:
            print('Not all the ', keyy , 'profiles have the same length!')
            raise

    for j in np.shape(xx)[1]:
        if len(np.unique(xx[:,j])) !=1:
            print( )

    fig, ax = plt.subplot(1,1, figsize = (5,5))
    ax.imshow(yy, **kwargs)
    ax.set_xticks()

    return fig, ax

### 1D profiles section

def get_meanprofile(df, keys, **kwargs):
    ''' '''
    #TODO: document

    dic = {}
    for key in keys:
        profiles =[]
        for i in df[df[key].notna()].index:
            profiles.append(df.at[i,key])
        profiles_mean= np.mean(profiles,axis=0)
        dic[key]= profiles_mean

    return dic

def get_stdprofile(df, keys, **kwargs):
    ''' '''
    #TODO: document

    dic = {}
    for key in keys:
        profiles =[]
        for i in df.index:
            profiles.append(df.at[i,key])
        profiles_std= np.std(profiles, **kwargs)
        dic[key]= profiles_std

    return dic


def get_varprofile(df, keys, **kwargs):
    ''' '''
    #TODO: document

    dic = {}
    for key in keys:
        profiles =[]
        for i in df.index:
            profiles.append(df.at[i,key])
        print(np.shape(profiles))
        profiles_std= np.var(profiles, **kwargs)
        dic[key]= profiles_std

    return dic



def get_cvprofile(df, keys, **kwargs):
    ''' '''
    #TODO: document

    dic = {}
    means = get_meanprofile(df, keys, **kwargs)
    stds = get_stdprofile(df, keys, **kwargs)
    from src.structure.numUtils import dividelists as div

    for key in keys:
        dic[key]= div(stds[key], means[key])

    return dic


