
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import cv2
import math

import src.image.imDisp as imd
import src.image.imUtils as imu
import src.image.imUtilsJ as imuj
import src.postprocess.APprofiles as ap
import src.analysis.pipelines as pip

def optimalsubplots(n, ratio=1):

    if n==1:
        row=1
        col=1

        return col,row

    row = int(np.sqrt(n))
    if row == np.sqrt(n):
        col = row
    else:
        col=row+1

    if col/row > ratio:
        while col/row > ratio:
            col -=1
            row = n // col
            row+= np.sign(n % col)


    elif col/row < ratio:
        while col/row < ratio:
            col +=1
            row = n // col
            row+= np.sign(n % col)

    return col, row




def convert(df, keys_to_convert, conversion_factor=1, suffix='_conv'):
    ''' Function that convert all the elements in keys from unit 1 to unit 2 using the conversion factor
    df: dataframe in which to look for the data
    keys_to_convert: column names of the data to convert
    unit1: units of the data to convert
    unit2: units in which convert the data
    conversion_factor: conversion factor
    suffix: suffix to add to the new columns
    '''



    return df




#Forgot how exactly it works, but it works!
def translateRotation(rotation, width, height):
    if (width < height):
        rotation = -1 * (rotation - 90)
    if (rotation > 90):
        rotation = -1 * (rotation - 180)
    rotation *= -1
    return round(rotation)

def getEllipseRotation(image, cnt):

    try:
        # Gets rotated bounding ellipse of contour
        ellipse = cv2.fitEllipse(cnt)
        centerE = ellipse[0]
        # Gets rotation of ellipse; same as rotation of contour
        rotation = ellipse[2]
        # Gets width and height of rotated ellipse
        widthE = ellipse[1][0]
        heightE = ellipse[1][1]
        # Maps rotation to (-90 to 90). Makes it easier to tell direction of slant
        rotation = translateRotation(rotation, widthE, heightE)
        # cv2.ellipse(image, ellipse, (23, 184, 80), 3)
        return rotation
    except:
        # Gets rotated bounding rectangle of contour
        rect = cv2.minAreaRect(cnt)
        # Creates box around that rectangle
        box = cv2.boxPoints(rect)
        # Not exactly sure
        box = np.int0(box)
        # Gets center of rotated rectangle
        center = rect[0]
        # Gets rotation of rectangle; same as rotation of contour
        rotation = rect[2]
        # Gets width and height of rotated rectangle
        width = rect[1][0]
        height = rect[1][1]
        # Maps rotation to (-90 to 90). Makes it easier to tell direction of slant
        rotation = translateRotation(rotation, width, height)
        return rotation


def get_rotation_angle(df, metod='fitellipse', includeprofile=True, gene=' ', orientation='A', bound=0.5):
    ''' Function that gets the rotation angle in order to align the gastruloids in the AP position
    df: dataframe containing the data
    metod: fitellipse, meananglebins
     '''

    import tifffile
    from scipy import ndimage

    df['rot_angle'] = 0
    df['rot_angle'] = df['rot_angle'].astype(pd.Int64Dtype())
    df['BBOX_rot'] = pd.NA

    if metod is 'fitellipse':
        for i in df[df['Mask'].notna()].index:

            filepath = df.at[i, 'absPath']
            if os.path.splitext(filepath)[1] == '.vsi':
                image = pip.get_middleslice(filepath)
            elif os.path.splitext(filepath)[1] == '.czi':
                pass
            elif os.path.splitext(filepath)[1] == '.tif':
                tif = tifffile.imread(filepath)
                # dimtif = np.shape(tif)
                # ich = np.argmin(dimtif)
                # nch = dimtif[ich]
                # if channel > nch:
                #     raise IndexError("Requested channel is ", channel, " but tiff image only contains ", nch,
                #                      'channels')
                # image = np.swapaxes(tif, ich, 0)
                # TODO: careful at which image I want to get, and the dimension of the tif image??
                #  (c x y for now because projection, what about z or t?)
                image = tif[2, :, :]

            mask_raw = np.array(df.at[i,'Mask']).astype(np.int32)
            mask = mask_raw[:, np.newaxis, :]
            angle = getEllipseRotation(image, mask)
            df.at[i,'rot_angle'] = -angle

            rotated = ndimage.rotate(image, df.at[i, 'rot_angle'], reshape=True)
            nx, ny = image.shape[:2]

            diag = np.sqrt(nx ** 2 + ny ** 2)
            x, y = rotated.shape[:2]
            newx = int(x/y* diag)
            newy = int(y/x* diag)

            df.at[i, 'cx'] = int(nx / 2)
            df.at[i, 'cy'] = int(ny / 2)
            df.at[i, 'cx_rot'] = int(newx / 2)
            df.at[i, 'cy_rot'] = int(newy / 2)
            df.at[i, 'dx_rot'] = int(newx - nx) / 2
            df.at[i, 'dy_rot'] = int(newy - ny) / 2

            mask = [rotate((ny / 2, nx / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in mask_raw]
            mask_arg = np.asarray([(m[0] + df.at[i, 'dy_rot'], m[1] + df.at[i, 'dx_rot']) for m in mask]).T
            A= [int(np.min(mask_arg[0])) - 100, int(np.max(mask_arg[0])) + 100, int(np.min(mask_arg[1])) - 100, int(np.max(mask_arg[1])) + 100]
            df.at[i, 'BBOX_rot'] = A

    if includeprofile:
        medax_raw = np.array(df.at[i, 'MedialAxis']).astype(np.int32)
        medax_rot = [rotate((ny / 2, nx / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in medax_raw]
        medax_trans = np.asarray([(m[0] + df.at[i, 'dy_rot'], m[1] + df.at[i, 'dx_rot']) for m in medax_rot])
        medax_arg = medax_trans.T

        if np.sign(medax_arg[0][1] - medax_arg[-1][1]) != np.sign(medax_raw[0][0] - medax_raw[-1][0]):
            df.at[i, 'rot_angle'] = 180 - angle

        df['flip'] = df[gene].apply(ap.APflip, mode=orientation, bound=bound)
        df.loc[ df['flip'], 'rot_angle'] = df.loc[ df['flip'], 'rot_angle']+180

    return df

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return qx, qy

def getbboxmax(data):
    '''Function to obtain the maximal dimensions of images given the list of their respective bbox determined elsewhere'''

    # Determine the maximal dimension of the images
    dx=[]
    dy=[]
    bboxx = []
    bboxy = []
    for b in data:
        dx.append(b[3]-b[2])
        dy.append(b[1]-b[0])
    dimx = int(np.max(dx))
    dimy = int(np.max(dy))
    # Calculate the corresponding bbox centered around the center of each bbox for each image
    for b in data:
            bboxx.append( [ (b[3]+b[2])/2 - dimx/2, (b[3]+b[2])/2 + dimx/2] )
            bboxy.append( [(b[1]+b[0])/2 - dimy/2, (b[1]+b[0])/2 + dimy/2] )

    return (dimy,dimx), bboxx, bboxy

def makeMontageAssay(df, explore=True, ratio=2, figheight = 5, channels=0, colors=[], mode = 'custom',
                     BBOX=False, MASK=True, CNT= True, MEDAX=True, SEG=False, ROT=False, ANNOT=None):
    ''' Function that plots all the images in the dataframe df including, if defined, the Mask contour, the contour, the medial axis, the segments and more
    df: dataframe containing the data
    explore: if True display informations such as the imageID, if False continuous montage to use in figures
    ratio: ratio of the number of columns over the number of rows in the subplot
    figheight: height of the figure
    channels: channels to use, only for tiff
    BBOX: if True the images are croped using the BBOX parameters in the dataframe
    mode: if 'data' only display the raw images; if 'complete', display all image features
    MASK, CNT, MEDAX, SEG: if True ,the corresponding features for the image are added to the plot
    '''

    #TODO: implement background removing outside of mask to erase other gastruloids
    #TODO: implement the choice of the extension
    #TODO: improve using a switch type of statement?
    #TODO: add the option to plot the corresponding profiles ?
    #TODO: correct the error for the rotation ! and implement an adjustemnet of the stretching when resizing different images to the same size so that there is a common scale
    #TODO: correct the correspondance k for axes and i for df in the case when i am only enumerate the value where some df data exist !!
    #TODO: correct the case where no channels is specifice: then all channels for IF and 1 channel for BF

    if mode is 'data':
        MASK = False
        CNT = False
        MEDAX = False
        SEG = False

    if mode is 'complete':
        MASK = True
        CNT = True
        MEDAX = True
        SEG = True

    if mode is 'plate':
        if not 'Well' in df.columns.values:
            print('Mode is plate, but no well info in dataframe')
            return 0,0,0
        if not 'Row' in df.columns.values:
            print('Mode is plate, but no row info in dataframe')
            return 0,0,0
        if not 'Col' in df.columns.values:
            print('Mode is plate, but no col info in dataframe')
            return 0,0,0
        ratio=1.5
        nImages=92
        ANNOT='Well'
    else:
        nImages = len(df)

    if not any(m in mode for m in ['data', 'complete', 'custom', 'plate']):
        print("Possible values for mode are data, complete, plate and custom. \n Using the data mode, only the raw images will be displayed. "
              "Use the complete mode to see all informations. Use custom to specify what you want to see or not.\n")

    if explore:
        a=1
    if ROT:
        if not 'rot_angle' in df.columns.values:
            print('Rotation angle not calculated. ')
        else:
            from scipy import ndimage

    import tifffile

    df=df.reset_index()
    print('making a montage of ', nImages, ' images...')
    (col, row) = optimalsubplots(nImages, ratio=ratio)
    print( row, 'x', col, ' subplots')

    dim=None

    fig, axs = plt.subplots(row, col)
    if np.size(axs)==1:
        axs =[axs]
    else:
        axs= axs.flat

    if mode is 'plate':
        K=[]
        for i in df.index:
            K.append( 12* (df.at[i,'Row']-1) + df.at[i,'Col']-1)
    else:
        K=range(len(df.index))

    for (k,i) in zip(K,df.index):
        # if explore:
        #     axs[k].set_title(df.at[i,'imageID'])

        filepath = df.at[i,'absPath']
        # print(filepath, os.path.exists(filepath))
        if not os.path.exists(filepath):
            axs[k].axis('off')
            continue

        if os.path.splitext(filepath)[1] == '.vsi':
            image=pip.get_middleslice(filepath)
        elif os.path.splitext(filepath)[1] == '.czi':
            pass
        elif os.path.splitext(filepath)[1] == '.tif':
            tif= tifffile.imread(filepath)
            tif = tif / np.amax(tif)
            tif = np.clip(tif, 0, 1)

            dimtif = np.shape(tif)
            ich  = np.argmin(dimtif)
            nch = dimtif[ich]
            if np.max(channels) > nch:
                raise IndexError("Requested channel is ", channels, " but tiff image only contains ", nch, 'channels')
            image= np.swapaxes(tif, ich, 0)
            image = np.moveaxis( image [channels, :,:], [0,1,2], [2,0,1])

        if ROT:
            if not 'rot_angle' in df.columns.values:
                print('No rotation angle as been calculated!')
            else:
                rotated = ndimage.rotate(image, df.at[i, 'rot_angle'], reshape=True)

                nx, ny = image.shape[:2]
                diag = np.sqrt(nx**2+ny**2)
                x,y = rotated.shape[:2]
                if len(channels)==1:
                    rotated = rotated[:,:,np.newaxis]
                newx = int(x / y * diag)
                newy = int(y / x * diag)
                final=np.zeros((newx,newy, len(channels)))
                final[ int(newx/2-x/2):int(newx/2+x/2), int(newy/2-y/2):int(newy/2+y/2)] = rotated
                if i in df[df['Mask'].notna()].index.values:
                    mask = np.array(df.at[i, 'Mask'])
                    background = imu.get_background(image, mask)
                    final[final==0] = background


                    ## Normal case IF
                    # mask = [rotate((ny / 2, nx / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in mask]
                    # mask_arg = np.asarray([(m[0]+int((newy-ny)/2), m[1]+ int((newx-nx)/2)) for m in mask]).T


                    ##IF BF images F15 !
                    mask = [rotate((nx / 2, ny / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in mask]
                    mask_arg = np.asarray([(m[0] + df.at[i, 'dx_rot'], m[1] + df.at[i, 'dy_rot']) for m in mask]).T

                    #TODO: move this part into the get_rot_angle to get the decalage once and for all to be used in
                    #other parts, almost done but problem of flip verification after everything else!
                    df.at[i,'BBOX_rot'] = [np.min(mask_arg[0]) - 100, np.max(mask_arg[0]) + 100, np.min(mask_arg[1]) - 100,
                                           np.max(mask_arg[1]) + 100]
                else:
                    df.at[i,'BBOX_rot'] = [0, final.shape[0],0,final.shape[1]]

            if np.shape(final)[-1] ==2:
                final =  np.dstack((final, np.zeros(final.shape[:2])))

            if colors:
                final_new = np.zeros(np.shape(final))
                for i, c in enumerate(colors):
                    final_new[:,:,c] = final[:,:,i]
                final=final_new
            # else:
            #     final_new = np.zeros(np.shape(final))
            #     if len(channels)==1:
            #         for i in range(3):
            #             final_new[:,:,i] = final[:,:,0]
            #     final=final_new

            out = (final/np.max(np.max(final)).astype(np.float))
            imd.dispIm(axs[k], out)

        else:
            final = image
            imd.dispIm(axs[k], image)

        if not dim:
            dim = np.shape(final)
            print('Images of size ', dim )


    if MASK:
        for (k, i) in zip(K, df[df['Mask'].notna()].index):
            mask = np.array(df.at[i,'Mask'])
            if ROT:
                ##Case where this is a Immunofluorescence image
                # mask = [rotate((ny / 2, nx / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in mask]
                # mask = np.asarray([(m[0] + df.at[i, 'dy_rot'], m[1] + df.at[i, 'dx_rot']) for m in mask])
                ##Case where this is a Brightfield image
                mask = [rotate((nx / 2, ny / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in mask]
                mask = np.asarray([(m[0] + df.at[i, 'dx_rot'], m[1] + df.at[i, 'dy_rot']) for m in mask])
            imd.addContour(axs[i], mask, color='y')

    if CNT:
        for (k, i) in zip(K, df[df['Contour'].notna()].index):
            cnt = np.array(df.at[i, 'Contour'])
            if ROT:
                cnt = [rotate((ny / 2, nx / 2), m, math.radians(df.at[i, 'rot_angle'])) for m in cnt]
                cnt = np.asarray([(m[0] + df.at[i, 'dy_rot'], m[1] + df.at[i, 'dx_rot']) for m in cnt])
            imd.addContour(axs[i], cnt, color='b')

    if MEDAX:
        for (k, i) in zip(K, df[df['MedialAxis'].notna()].index):
            med = np.array(df.at[i, 'MedialAxis'])
            if ROT:
                med = [rotate( (ny/2,nx/2), m, math.radians(df.at[i,'rot_angle'])) for m in med]
                med = np.asarray([(m[0] + df.at[i,'dy_rot'], m[1] + df.at[i,'dx_rot']) for m in med])

            imd.addContour(axs[i], med, color='r', linewidth=2)

    if SEG:
        for (k, i) in zip(K, df[df['Segments'].notna()].index):
            S = df.at[i, 'Segments']
            S = np.array(S).squeeze()
            for seg in range(np.shape(S)[0]):
                if seg%10==0:
                    axs[i].plot([S[seg, 0, 1], S[seg, 1, 1]], [S[seg, 0, 0], S[seg, 1, 0]], c='g')

    if BBOX:
        if ROT:
            dim , bbx, bby = getbboxmax(df['BBOX_rot'])
            for (k,bx,by) in zip(K,bbx,bby):
                axs[k].set_xlim(bx[0], bx[1])
                axs[k].set_ylim(by[0], by[1])
        else:
            dim , bbx, bby = getbboxmax(df['BBOX'])
            for (k, bx, by) in zip(K,bbx, bby):
                axs[k].set_xlim(bx[0], bx[1])
                axs[k].set_ylim(by[0], by[1])

    for k in list(set(range(col*row)) - set(K)):
        axs[k   ].axis('off')

    if not dim:
        dim =[100,100]
    print('Final display of size ', dim)
    if ANNOT:
        print('annotate!')
        if BBOX:
            for (k, i, bx, by) in  zip(K, df[df[ANNOT].notna()].index, bbx, bby):
                axs[k].text(bx[0] + 0.1 * dim[1], by[0] + 0.9 * dim[0], df.at[i, ANNOT], color='w')
        else:
            for (k, i) in zip(K, df[df[ANNOT].notna()].index):
                axs[k].text(0.1 * dim[1], 0.9 * dim[0], df.at[i, ANNOT], color='r')

    fig.set_size_inches(figheight*col/row*dim[1]/dim[0], figheight)
    alpha =0
    delta = 0
    if explore:
        alpha = 0.05
        delta = 0.2
    fig.subplots_adjust(left=alpha, right=1-alpha, top=1-alpha, bottom=alpha)
    fig.subplots_adjust(wspace=delta, hspace=delta)

    return fig, axs, dim


def pipeline(df):
    ''' '''
    #TODO: implement a regular pipeline of post-process analysis and maybe save directly inside this
    # so that it is possible to compare different dataset or different plates more easily

# def makeMontageOut(df, ratio=5, channel=0, rotation=False,  BBOX=False):
#     ''' Function that plots all the images in dataframe'''
#
#     #TODO: implement rotation using the direction of gastruloid
#     #TODO: implement BBOX
#     #TODO: implement background removing outside of mask to erase other gastruloids
#     #TODO: implement the choice of the extension
#     #TODO: improve using a switch type of statement?
#
#     import tifffile
#
#     nImages = len(df)
#     print('making a montage of ', nImages, ' images...')
#     (col, row) = optimalsubplots(nImages, ratio=ratio)
#     print( row, 'x', col, ' subplots')
#
#     fig, axs = plt.subplots(row, col, figsize=(10*col/row, 10))
#     axs = axs.flat
#     for k,i in enumerate(df.index):
#         filepath = df.at[i,'absPath']
#         if os.path.splitext(filepath)[1] == '.vsi':
#             image=pip.get_middleslice(filepath)
#             imd.dispIm(axs[k], image)
#         elif os.path.splitext(filepath)[1] == '.czi':
#             pass
#         elif os.path.splitext(filepath)[1] == '.tif':
#             image= tifffile.imread(filepath)[channel, :,:]
#             imd.dispIm(axs[k], image)
#
#         if k==1:
#             dim = np.shape(image)
#
#     for kk in range(k,col*row):
#         axs[kk].axis('off')
#
#     fig.set_size_inches(3*col/row*dim[1]/dim[0], 3)
#     print(dim)
#     fig.subplots_adjust(left=0, right=1, top=1, bottom=0)
#     fig.subplots_adjust(wspace=0, hspace=0)
#
#     return fig, axs
#
