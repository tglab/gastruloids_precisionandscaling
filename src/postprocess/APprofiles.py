import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def APflip(data, mode ='A', bound=0.5):
    ''' '''
    #TODO: comment
    flip = False
    n = np.int(np.floor(len(data)*bound))
    data1 = data[:n]
    data2 = data[-n - 1:-1]
    if mode=='A':
        if np.sum(data1) < np.sum(data2):
            flip = True
    elif mode=='P':
        if np.sum(data1) > np.sum(data2):
            flip = True
    return flip

def fliprofiles(df,  keys_to_flip, genes=[], pos = '' ):
    ''' Function to flip all the profiles defined by keys_to_flip that have a 'flip' parameter set to True
     flip is deleted at the end, so all the keys need to be provided
     df: dataframe on which to operate the flippings
     keys_to_flip: define the name of columns that are to be flipped,
                    should contained 1D profile, ie array of dimension 1xn
     genes:? option that find all the profiles starting with gene in genes such as geneDAPI geneArea etc. to account for different renormalization metod?
     #TODO: do the flipping of profile before any renormalization !!
     pos:? A way to define in which sense I want to aligne the profile ? AP or PA
     #TODO: ensure that the AP or PA orientation is defined at the moment where the flip parameter is calculated'''

    if 'flip' not in df.columns.values:
        print('No flip parameter, exit')
        return 0

    for i in df.index:
        if not df.at[i, 'flip']:
            continue

        for key in keys_to_flip:
            df.at[i, key] = np.flip(df.at[i, key])

    del df['flip']

    return df

def findextrema10(profile, k=10):
    ''' Find the extrema of a given 1D profile using the k% minimal and maximal points. '''
    #TODO: automatize the application of the function to a list of key OR
    # change all function here so that pipelines are a serie of df.apply functions?

    ordered = np.argsort(profile)
    Imin = np.mean(profile[ordered[:int(len(ordered)/k)]])
    Imax = np.mean(profile[ordered[-int(len(ordered)/k):]])

    return Imin, Imax

def findboundary_extrema(profile, k=10, xmin=0.0, xmax=1.0):
    '''Determine the position of a boundary defined as the place
    where the intensity is going from under to above (Imax+Imin)/2 '''

    #TODO: automatize the application of the function to a list of key OR
    # change all function here so that pipelines are a serie of df.apply functions?
    #TODO: test

    Imin, Imax= findextrema10(profile, k)
    imin = int(len(profile)*xmin)
    imax = int(len(profile)*xmax)

    if len( np.argwhere(np.diff(np.sign(profile[imin:imax]-(Imax+Imin)/2)))) >0:
        iboundary = imin + np.argwhere(np.diff(np.sign(profile[imin:imax]-(Imax+Imin)/2)))
    Iboundary = [profile(x) for x in xboundary]

    return iboundary, Iboundary

def findboundary_fitsigmoid(profile, xbin, xmin=0.0, xmax=1.0):
    '''Determine the position of a boundary defined as the maximal derivative by fitting the profile by a sigmoid function '''

    #TODO: automatize the application of the function to a list of key OR
    # change all function here so that pipelines are a serie of df.apply functions?
    #TODO: test
    from scipy.optimize import curve_fit

    def sigmoid(x, L, x0, k, b):
        y = L / (1 + np.exp(-k * (x - x0))) + b
        return (y)

    Imin, Imax= findextrema10(profile, k)
    p0 = [Imax - Imin, np.median(xbin[xmin:xmax]), 1, Imin] # this is an mandatory initial guess
    popt, pcov = curve_fit(sigmoid, xbin, geneu, p0, method='dogbox')

    if len( xmin + np.argwhere(np.diff(np.sign(geneu[xmin:xmax]-(Imax+Imin)/2)))) >0:
        xboundary = xmin + np.argwhere(np.diff(np.sign(geneu[xmin:xmax]-(Imax+Imin)/2)))
    Iboundary = [profile(x) for x in xboundary]

    return xboundary, Iboundary






def normalize_bydiv(df, keys_to_normalize, keys_of_norms):
    ''' Normalize all profiles specified by keys_to_normalize using the values in keys_of_norms by simple division,
    either term by term of by a number. The resulting column of key1 normalized by key2 will be key1_key2.
    #TODO: implement other types of normalization ?
    Each column of keys_to_normalize should contain 1xn array and keys_of_norm should contain 1xn array or single number
     #TODO define whether it should be int or float if single number
     #TODO: implement to if that makes the difference of these two cases
     df: dataframe for which the columns are modified
     keys_to_normalize: columns key on which to apply renormalization
     keys_of_norms: columns key oby which to renormalize
     '''

    from src.structure.numUtils import dividelists as div

    for key1 in keys_to_normalize:
        for key2 in keys_of_norms:
            newkey = key1+'_'+key2
            print(newkey)
            df[newkey] = df.apply(lambda x: div(x[key1],x[key2]), axis=1)

    return df

def smooth(df, keys, suffix= '_smooth', nrm=3):
    ''' '''

    #TODO: implement and document
    #TODO: decide if smoothed is a new column : in what order am I supposed to proceed?
    # First renomalize (by area ie), then smooth ?

    for key in keys:
        df[key + suffix] =pd.NA
        for i in df.index:
            if nrm == 1:
                geneu = np.array(df.at[i, key])
            else:
                geneu = np.convolve(df.at[i, key], np.ones(nrm) / nrm, mode='valid')
            df.at[i,key+suffix] = geneu

    return df

## Fonctions to display the profiles: plot, scatter, or imshow
#TODO: modify the function to return the axe(s) instead of fig + ax to gain flexibility


def plotprofiles(df, keys_x, keys_y, **kwargs ):
    ''' Plot profiles y = f(x) for all combinations of keys_x and keys_y
     df: dataframe from which to extract the profiles
     keys_x: keys to use as the abciss
     keys_y: keys to use as the ordinate
     **kwargs: supplementary parameters to enter the matplotlib plot function
     '''

    fig, axs = plt.subplots(len(keys_x), len(keys_y), figsize=(10,10), squeeze=False)
    for ix in df.index:
        for i,keyx in enumerate(keys_x):
            for j,keyy in enumerate(keys_y):
                x=df.at[ix,keyx]
                y=df.at[ix,keyy]
                axs[i,j].plot(x,y, **kwargs)
                axs[i,j].set_xlabel(keyx)
                axs[i,j].set_ylabel(keyy)

    return fig,axs

def scatterprofiles(df, keys_x, keys_y, **kwargs):
    ''' Scatter profiles y = f(x) for all combinations of keys_x and keys_y
     df: dataframe from which to extract the profiles
     keys_x: keys to use as the abciss
     keys_y: keys to use as the ordinate
     **kwargs: supplementary parameters to enter the matplotlib plot function
     '''

    #TODO: improve the return so that parameters such as the xscale can be changed afterwards
    #TODO: unify with other functions such as the plotprofiles function

    fig, axs = plt.subplots(len(keys_x), len(keys_y), figsize=(10,10), squeeze=False)
    for ix in df.index:
        for i,keyx in enumerate(keys_x):
            for j,keyy in enumerate(keys_y):
                x=df.at[ix,keyx]
                y=df.at[ix,keyy]
                axs[i,j].scatter(x,y, **kwargs)
                axs[i,j].set_xlabel(keyx)
                axs[i,j].set_ylabel(keyy)

    return fig, axs

def imshowprofiles(df, keys_y, keyx=[], tickY = [], **kwargs):

    ''' Display all profiles of keys_y under the appearance of an image. '''

    #TODO: complete the doc and comment
    #TODO: case where the kwargs would be different for different keys_y ?? Is it possible?
    #TODO: modify when all the x possible will be also stored in the dataframe! !! And make it possible to have several keysx
    # Then the xlabel by default will be the name of the columne containing keyx

    height = 20
    fig, axs=  plt.subplots(1,len(keys_y), figsize=(height,len(keys_y*height)))

    for k,key in enumerate(keys_y):
        if len(keys_y)==1:
            ax=axs
        else:
            ax=axs[k]
        img = []
        img.append(df.at[df.index[0], key])
        if not keyx:
            x=range(len(img))
        else:
            x=df.at[df.index[0], keyx]
        if np.shape(img)[1] != len(x):
            print('error: the length of the specified x axis is not adequate to the length of keys_y')
        X= np.linspace(x[0],x[-1],10)
        X= [str(round(x, 1)) for x in X]

        for i in df.index[1:]:
            if len(df.at[i, key]) > np.shape(img)[1]:
                print('error size')
            img.append(df.at[i, key])
        ax.imshow(img, **kwargs)
        ax.set_title(key)
        if not tickY:
            ax.set_yticks([])
        else:
            ax.set_yticks(np.arange(len(df)))
            ax.set_yticklabels(df[tickY].values)
        ax.set_xticks(np.linspace(0,np.shape(img)[1],10))
        ax.set_xticklabels(X)

    return fig, axs

