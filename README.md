# Gastruloid precision and scaling

## Description
This code contains pipelines of analysis for microscopy images of gastruloids, to extract features such as length, volume and profiles of expression.
It also contains functions to analysis further profiles of expression of population of gastruloids. 

These pipelines were the ones used in the arXiv paper https://arxiv.org/abs/2303.17522.
To find all the corresponding datasets, go to https://zenodo.org/record/8108188.

## Installation

    conda create -n gasp python=3.6.13
    conda activate gasp
    conda install -f imageanalysis.yml


## Usage

The pipeline of image analysis is composed of different steps: 
1. Contour
2. Midline 
3. Slicing
These steps are summarized in the following image 
<img src="/images/pipeline.png" alt="pipeline" width="10%" height="10%" title="Analysis Pipeline">

For this different features can be extracted such as length or volume. For gastruloids stained with IF, 1D profiles of expression can also be calculated. 

Depending on the type of images, either BF or IF, the analyze pipeline is slightly different. An example of usage of the code for each type of files is available in the notebooks : 
- [Demo: analysis of an BF stack](/notebooks/fullanalysis_onBF.ipynb)
- [Demo: analysis of an IF stack](/notebooks/fullanalysis_onstack.ipynb)


## Roadmap
Add, clean, comment and document the code allowing for the analysis of a cohort of gastruloids in batch.

## Project status
This project is under development. 
