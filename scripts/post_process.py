import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from datetime import date
import time


# parameters
nrm=1  # smoothing parameters
xbin = [0.01] + [0.0025 + x/200 for x in range(2*2,98*2, 1)] + [0.99] # coordinates of bins



# define the saving folder and the saving function

results_folder = '/mnt/pbf1/Melody/results_IF/RE-A/'

if not os.path.exists(results_folder):
    os.mkdir(results_folder)
global_analysis = 'v1'
save_folder = results_folder + global_analysis + '/'
if not os.path.exists(save_folder):
    os.mkdir(save_folder)

def figsave(fig, name, dpi=600):
    todayfolder = os.path.join(save_folder, date.today().strftime("%Y%m%d"))
    if not os.path.exists(todayfolder):
        os.mkdir(todayfolder)
    filename = os.path.join(todayfolder, name)
    k = 0
    while os.path.exists(filename):
        filename = filename + str(k).zfill(1)
    fig.savefig(filename + '.jpg', dpi=dpi)



# Load the dataframe with results
main_folder = '/mnt/pbf1/Melody/results_IF/RE-A/'
if not os.path.exists(main_folder):
    os.mkdir(main_folder)

df_name = '_data_v1'
file = os.path.join(main_folder, df_name + '.json')
tstart = time.time()
df_analysis_raw = pd.read_json(file, orient='split')
tend = time.time()
print('Duration : ', (tend - tstart) / 60, 'min')

# Remove the gastruloid for which the analysis was not completed
df_analysis_keep = df_analysis_raw[df_analysis_raw['Flag']=='Keep']
df_analysis = df_analysis_keep.copy()
n_tot = len(df_analysis_raw)
n_keep = len(df_analysis)
print('# of images analyzed : ', n_tot, ' # of images kept :', n_keep)

print( n_keep/n_tot*100 , '% of images analyzed without issues' )

#

# import src.postprocess.popgeneral as popg
# from importlib import reload
# reload(popg)
#
# df=df_analysis.sample(10)
# df = popg.get_rotation_angle(df, includeprofile=False, gene='Sox2', orientation='P', bound=0.3)
#
# fig, _ , dim= popg.makeMontageAssay(df, channels=[0,1,2], colors=[0,1,2], ratio=2,  explore=False, mode='data',
#                            figheight= 20, CNT=False, MEDAX=False, BBOX=True, SEG=False, ROT=True, BCK=False)
# plt.show()
#
# figsave(fig, 'montage_allgenes')
#
#
# reload(popg)
#
# df=df_analysis.sample(100)
#
#
# df = popg.get_rotation_angle(df, includeprofile=True, gene='Sox2', orientation='P', bound=0.3)
#
# fig, _ , dim= popg.makeMontageAssay(df, channels=[0,1,2], colors=[0,1,2], ratio=1.0,  explore=False, mode='data',
#                            figheight= 20, CNT=False, MEDAX=False, BBOX=True, SEG=False, ROT=True, BCK=False)
# plt.show()
#
# figsave(fig, 'montage_allgenes_100')

# for exp in df_analysis.expID.unique():
#     df = df_analysis[df_analysis.expID == exp].copy()[:1]
#     fig, _, dim = popg.makeMontageAssay(df, channels=[0, 1, 2], colors=[2, 1, 0], ratio=2, explore=False,
#                                         figheight=20, CNT=True, MEDAX=True, BBOX=False, SEG=True, ROT=False,
#                                         ANNOT='imageID')
#     plt.show()
#
#     figsave(fig, 'montage_keep_' + exp)


# Get the outliers ?
#
# print(len(df_analysis))
# outliers = []
#
# outliers +=  [ 'RE-A-B1_S' + s for s in ['35'] ]
# outliers +=  [ 'RE-A-B2_S' + s for s in [] ]
# outliers +=  [ 'RE-A-C_S' + s for s in ['05', '15', '16','39'] ]
# outliers +=  [ 'RE-A-F_S' + s for s in ['17','46', '28'] ]
#
# df_analysis = df_analysis[~df_analysis['imageID'].isin(outliers)]
# df_analysis = df_analysis[~df_analysis['expID'].isin(['RE-A-B2'])]
# print(len(df_analysis))



# Process the data :
# Calculate width and thickness (could be added directly in the analysis pipeline ?)
df_analysis['thick'] = pd.NA
df_analysis['thick_um'] = pd.NA
df_analysis['Widths_um'] = pd.NA
df_analysis['Widths'] = pd.NA
df_analysis['Lengths_um'] = pd.NA
df_analysis['Volumes_um'] = pd.NA
for i in df_analysis.index:
    S = df_analysis.at[i, 'Segments']
    S = np.squeeze(np.array(S))
    get_thickness = lambda j: np.power(np.power(S[j, 0, 1] - S[j, 1, 1], 2) + np.power(S[j, 0, 0] - S[j, 1, 0], 2), 0.5)
    thickness = []
    thickness_um = []
    #         for k in range(1,len(S)-1):
    for k in range(len(S)):
        thickness.append(get_thickness(k))
        thickness_um.append(get_thickness(k) * df_analysis.at[i, 'um_per_pixel'])
    df_analysis.at[i, 'thick'] = thickness
    df_analysis.at[i, 'Widths'] = np.add(df_analysis.at[i, 'thick'][1:],
                                         df_analysis.at[i, 'thick'][:-1]) / 2


# Flip and normalize the profiles
import src.postprocess.APprofiles as ap
keys_to_flip = ['Sox2', 'Cdx2', 'Bra', 'Foxc1', 'DAPI', 'Lengths', 'Areas', 'Volumes', 'thick', 'Widths']
df_analysis['flip'] = df_analysis['Sox2'].apply(ap.APflip, mode='P', bound=0.3)
df_analysis = ap.fliprofiles(df_analysis, keys_to_flip)
df_analysis = ap.normalize_bydiv(df_analysis, ['Sox2', 'Cdx2', 'Bra', 'Foxc1', 'DAPI'], ['Areas', 'Volumes', 'DAPI'])

# define xbin : to be removed because should be directly in the main text
df_analysis['xbin'] = pd.NA
for i in df_analysis.index:
    df_analysis.at[i, 'xbin'] = xbin

# smooth profiles and reduce xbin  ; if nrm=1, this does nothing.
keys_to_smooth = ['Sox2_DAPI', 'Cdx2_DAPI', 'Sox2_Areas', 'Cdx2_Areas', 'Sox2_Volumes', 'Cdx2_Volumes',
                         'Bra_DAPI', 'Foxc1_DAPI', 'Bra_Areas', 'Foxc1_Areas', 'Bra_Volumes', 'Foxc1_Volumes']
df_analysis = ap.smooth(df_analysis, keys_to_smooth, nrm=nrm)
if nrm > 1:
    df_analysis['xbin_smooth'] = df_analysis['xbin'].apply(
        lambda x: x[np.int(np.floor(nrm / 2)):-np.int(np.floor(nrm / 2))])
else:
    df_analysis['xbin_smooth'] = df_analysis['xbin']


# Convert all data from pixel to µm
df_analysis['Length_MA_um'] = df_analysis['Length_MA'] * df_analysis['um_per_pixel']
df_analysis['Lengths_MA_um'] = df_analysis['Lengths'].apply(lambda x: [xx * df_analysis['um_per_pixel'] for xx in x])
df_analysis['thick_um'] = df_analysis['thick'].apply(lambda x: [xx * df_analysis['um_per_pixel'] for xx in x])
df_analysis['Widths_um'] = df_analysis['Widths'].apply(lambda x: [xx * df_analysis['um_per_pixel'] for xx in x])
df_analysis['Volume_MA_mm3'] = df_analysis['Volume_MA'] * df_analysis['um_per_pixel'] ** 3
df_analysis['Volume_eq_mm3'] = df_analysis['Length_MA'] ** 3 * np.pi / 6 * df_analysis['um_per_pixel'] ** 3 / (10 ** 9)

df_analysis['Area_um2'] = df_analysis['Areas'].apply(np.sum)
df_analysis['Area_um2'] = df_analysis['Area_um2'] * df_analysis['um_per_pixel'] ** 2

# Save the processed dataframe
df_analysis.to_json(os.path.join(main_folder, df_name + '_postpro.json'), orient='split')



