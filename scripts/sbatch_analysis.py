import os, sys, time
sys.path.append(os.getcwd())

import src.structure.global_structure as gs

import multiprocessing as mp
import numpy as np
import pandas as pd

gs.define_data_ext('.vsi')
gs.define_data_folder('/mnt/pbf1/Melody/gastruloid_images/rnaseq/RN2/')
gs.define_results_folder('/mnt/home/results/analysis_sizes/')
gs.define_analysis_name('RN2-2')

df_analysis = gs.load_analysis_dataframe()
df_analysis = df_analysis[df_analysis['Time'].isin([48])]
# df_analysis = df_analysis[df_analysis['Plate'].isin(['GMA'])]
#df_analysis = df_analysis[df_analysis['Row'].isin([3])]
# df_analysis = df_analysis[df_analysis['nCells'].isin([500])]

#TODO: add a criteria : abspath not emtpy
#TODO: initialize absPath with pd.NA

df_analysis['channels'] = pd.Series(dtype=np.dtype(dict))
dict_channels = {'BF':0}
for i in df_analysis.index:
    df_analysis.at[i,'channels']= dict_channels

print(df_analysis)

name_analysis = 'RN2'

gs.save_data_dataframe(df_analysis, name=name_analysis)

folder_analysis = os.path.join(gs.RES_FOLDER, gs.ANALYSIS_NAME, name_analysis)
if not os.path.exists(folder_analysis):
    os.mkdir(folder_analysis)

import src.analysis.pipelines as pipelines
import pandas as pd



def analysis_df(df):

    #TODO : find a way to not have to initialize every column in the dataframe by smart use of merge update etc

    print(df.index)

    df['Flag'] = None
    df['Saved'] = None
    df['Mask'] = None
    df['Contour'] = None
    df['BBOX'] = None
    df['MedialAxis'] = None
    df['Segments'] = None
    df['Length_MA'] = None
    df['Volume_MA'] = None
    df['Volumes'] = None
    df['Lengths'] = None
    df['Thicknesses'] = None
    df['Areas'] = None
    df['BF'] = None

    for i, row in df.iterrows():
        ID = row['imageID']
        filename =  folder_analysis +  '/temp_' + ID +'.json'
        print(ID, filename)
        if os.path.exists(filename):
            df_read = pd.read_json(filename,orient='split')
            df_read.index =[i]
            print(df_read)

            if df_read.at[i,'Flag'] == 'Keep':
                df.update(df_read)
                print('Analysis loaded for row', i, ID)
                continue

        print(row['absPath'])
        try:
            image, Flag, mask_cnt, cnt, BBOX, MedialAxis, length, segments_bound,volume, volumes, lengths, thicknesses = \
                pipelines.morphological_analysis_file(row['absPath'])
            print('Analysis completed for row', i , ID)
            print(type(volume))
            row['Flag'] = Flag
            row['Mask'] = mask_cnt
            row['Contour'] = cnt
            row['BBOX'] = BBOX
            row['MedialAxis'] = MedialAxis
            row['Segments'] = segments_bound
            row['Length_MA'] = length
            row['Volume_MA'] = volume
            print(type(row['Volume_MA']))
            row['Volumes'] = volumes
            row['Lengths'] = lengths
            row['Thicknesses'] = thicknesses

        except:
            print('Analysis error')
            row['Flag'] = 'AnalysisError'

        # if row['Flag'] == 'Keep':
        #     print('IF analysis!')
        #     row_out = pipelines.profiles_analysis(row)
        #     row = row_out


        try:
            row.to_frame().transpose().to_json(filename, index=False, orient='split')
            s='done'
        except:
            s='failed'

        row['Saved'] = s

        df.at[i]  = row

    return df

# ncores = 4
# ncores = os.environ( 'SLURM_CPUS_ON_NODE')
ncores = mp.cpu_count()

df_chunks = np.array_split(df_analysis ,ncores)

t=time.time()

pool = mp.Pool( ncores)
processed_df = pd.concat(pool.map(analysis_df, df_chunks), ignore_index=True)
print( type(processed_df.at[17,'Volume_MA']))

pool.close()
pool.join()

# processed_df = analysis_df(df_analysis)

print(processed_df)
# processed_df.to_json( os.path.join(folder_analysis,'_test.json'), orient='split')
gs.save_data_dataframe(processed_df, name=name_analysis)
print(( time.time() - t )/60,'min')

