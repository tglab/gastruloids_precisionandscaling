import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sbs
import os

import matplotlib.cm as cm
from matplotlib.colors import Normalize
plt.rcParams.update({'font.size': 15})

import src.postprocess.poppipelines as poppip
import src.structure.global_structure as gs

main_folder = '/mnt/home/results/experror/rawintensity'
name_analysis = '_data_RC01_v1'
name_results = 'RC-tot'

df, results_folder, figsave = poppip.initialize(main_folder, name_results, name_analysis)

df =df[:3]
df, df_new, _, figm, figh = poppip.clean_dataframe(df, showhist=True)
figsave(figm, 'montageraw')
figsave(figh, 'flaghist')

df_new.to_json( os.path.join(main_folder, name_analysis + '_' + name_folder +'.json'), orient='split')





