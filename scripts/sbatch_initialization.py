

import src.structure.global_structure as gs


gs.define_data_ext('.vsi')
gs.define_data_folder('/mnt/pbf1/RNAseq_scaling/')
gs.define_results_folder('/mnt/home/results/RNAseq/')
gs.define_analysis_name('RNAseq1')

gs.initialization()

df = gs.load_df_plates()
print(df)

df = gs.load_df_timepoints()
print(df)

df = gs.load_df_analysis()
print(df)



