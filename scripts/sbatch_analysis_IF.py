import os, sys, time
sys.path.append(os.getcwd())

import multiprocessing as mp
import numpy as np
import pandas as pd


folder_analysis='/mnt/home/results/experror/rawintensity'
df_IF = pd.read_json( os.path.join(folder_analysis, '_data_RC03.json') , orient='split')

# folder_analysis = '/mnt/home/results/analysis_IF/RA_MM_Cdx2'
# df_IF = pd.read_json( os.path.join(folder_analysis, '_data.json') , orient='split')

# df_IF = df_IF[17:18]

# df_IF.absPath = df_IF.absPath.apply(lambda s: s.replace('/results_IF/', '/results/analysis_IF/'))

#folder_analysis = '/mnt/home/results/experror/volumemeasurement/phall_50_leah'
# gast = '07'
#folder_analysis = '/mnt/home/results/experror/rotation_v2/'

#df_IF = pd.read_json( os.path.join(folder_analysis, 'rotationv2.json') , orient='split')

print(df_IF)

from datetime import date
today =date.today().strftime("%Y%m%d")

import src.analysis.pipelines as pipelines
import tifffile

def analysis_df(df):

    print(df.index)

    df['Flag'] = 'Initial'
    df['Saved'] = None
    df['Mask'] = None
    df['Contour'] = None
    df['BBOX'] = None
    df['MedialAxis'] = None
    df['Segments'] = None
    df['Length_MA'] = None
    df['Volume_MA'] = None
    df['Volumes'] = None
    df['Lengths'] = None
    df['Areas'] = None

    df['DAPI'] = None
    df['Sox2'] = None
    df['Cdx2'] = None
    df['Bra'] = None
    df['Foxc1'] = None

    for i, row in df.iterrows():
        ID = row['imageID']
        filename =  folder_analysis +  '/' + today + '/temp_' + ID +'.json'
        imagepath = row['absPath']

        ##Temporary because location of files have been changed!
        # imagepath = '/mnt/home/home/results/analysis_IF/RA_MM_Cdx2/n300/' + os.path.basename(imagepath)
        # row['absPath']=imagepath

        if not os.path.exists(imagepath):
            row['Flag'] = 'ImageNotFound'
            row = row.to_frame().transpose()
            row.to_frame().transpose().to_json(filename, index=False, orient='split')
            row.index = [i]
            df.update(row)
            continue

        imtiff = tifffile.imread(imagepath)
        dapich = row['channels']['DAPI']
        image = np.int16(imtiff[dapich,:,:])

        nbin = 200

        try:
            coef, Flag, maskarg, cnt, BBOX, MedialAxis, length, segments_bound, volume, volumes, lengths =  \
                pipelines.morphological_analysis_image(image, row['um_per_pixel'], ndiv =nbin)
            print('Analysis completed for row', i , ID)
            row['Flag'] = Flag
            row['Mask'] = maskarg
            row['Contour'] = cnt
            row['BBOX'] = BBOX
            row['MedialAxis'] = MedialAxis
            row['Segments'] = segments_bound
            row['Length_MA'] = length
            row['Volume_MA'] = volume
            row['Volumes'] = volumes
            row['Lengths'] = lengths

        except:
            print('Analysis error')
            #row['Flag'] = 'AnalysisError'

        print(row['Flag'])

        if row['Flag'] == 'Keep':
            row_out = pipelines.profiles_analysis_curves(row)
            row=row_out

        print('yeah!')

        try:
            row.to_frame().transpose().to_json(filename, index=False, orient='split')
            s='done'
        except:
            s='failed'

        row['Saved'] = s
        row = row.to_frame().transpose()
        row.index = [i]

        print('save', s)

        df.update( row)
        print(row)
        print('df updated, exit \n')

    return df

# ncores = 2
# ncores = os.env.SLURM_CPUS_ON_NODE
ncores = mp.cpu_count()
# ncores = 1

if not os.path.exists( os.path.join(folder_analysis, today )):
    os.mkdir(os.path.join(folder_analysis, today ))

df_test = df_IF
if 'path' in df_test.columns:
    print('old col names')
    df_test['absPath'] = df_test['path']

df_chunks = np.array_split(df_test ,ncores)

t=time.time()
pool = mp.Pool( ncores)
processed_df = pd.concat(pool.map(analysis_df, df_chunks), ignore_index=True)
pool.close()
pool.join()
# processed_df = analysis_df(df_IF)
print(( time.time() - t )/60,'min')

print(processed_df)
processed_df.to_json( os.path.join(folder_analysis,'_data_v3.json'), orient='split')



