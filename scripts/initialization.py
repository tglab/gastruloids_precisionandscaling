import sys, os

sys.path.extend( [os.path.abspath(os.getcwd()) ])

from src.structure import global_structure as gs
import javabridge

import pandas as pd

# data_folder = sys.argv[1]
# results_folder = sys.argv[2]
# analysis_name= sys.argv[3]
# plate_categories = sys.argv[4].split(',')
#
# gs.define_analysis_name(analysis_name)
# gs.define_data_folder(data_folder)
# gs.define_results_folder(results_folder)
# gs.define_data_folder('/mnt/pbf1/Leah/211208-imagescountFACS/')
# gs.define_results_folder('/mnt/home/results_Leah/')
# gs.define_analysis_name('LeahCounting')
gs.define_data_folder('/mnt/pbf1/Melody/gastruloid_images/rnaseq/RN2')
# gs.define_data_folder('/mnt/pbf1/Melody/gastruloid_images/FACS')
gs.define_results_folder('/mnt/home/results/analysis_sizes')
gs.define_analysis_name('RN2-2')


gs.define_data_ext('.vsi')

# df_summ = gs.initiate_plates_summary(plate_categories=['F13'])
df_summ = gs.initiate_plates_summary()
print(df_summ)
# gs.create_subfolders()
gs.correct_times()


gs.initiate_analysis_dataframe()
gs.update_wellinfos( gs.PLATE_NCELLS, name=gs.ANALYSIS_NAME, colname='nCells')
gs.find_abspath_files()

df_analysis = gs.load_analysis_dataframe()

print(df_summ)
print(df_analysis)

javabridge.kill_vm()


