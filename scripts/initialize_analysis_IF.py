import os

import javabridge as jv
import bioformats
import src.image.imUtils as imu

import numpy as np
import pandas as pd
import tifffile

import matplotlib.pyplot as plt

import time

jv.start_vm(class_path=bioformats.JARS)
imu.init_logger()

gastrus = ['05']

for gastru in gastrus:

    # folder= '/mnt/pbf1/Melody/gastruloids_IF/220425_variance/220422-mounting/RA04-PBS-one/'  + gastru + '/'
    folder= '/mnt/pbf1/Melody/gastruloids_IF/220425_variance/220422-mounting/RA04-PBS/'


    files= os.listdir(folder)
    files = [f for f in files if 'Airyscan' in f ]

    print(files)

    magn= ['10x']
    metod = ['AS']
    dict_channel = {'Foxc1':0 , 'Sox2':1, 'DAPI':2}

    aliases = ['RA04_PBS' + gastru + '_AS']
    ncells = [300]

    target = '/mnt/home/home/results/experror/rotation/'
    if not os.path.exists(target):
        os.mkdir(target)

    df_IF = pd.DataFrame(columns=['imageID', 'absPath',  'magn',  'metod' ,'um_per_pixel', 'Nx', 'Ny', 'channels', 'ncells'])

    tst= time.time()

    for i,f in enumerate(files):

        print('f', f)

        metadata = imu.get_metadata(folder + f)
        Ns = metadata['Nseries']
        print("# series = ", Ns)
        um_per_pixel = metadata['dx']
        Nc = metadata['Nch']
        Nx = metadata['Nx']
        Ny = metadata['Ny']

        tiff_folder  = os.path.join(target, 'PBS' + gastru + '_' + magn[0] + '_' + metod[0])
        if not os.path.exists(tiff_folder):
            os.mkdir(tiff_folder)

        for s in range(Ns):

            savefile = tiff_folder + '/' + aliases[0] + '_S' + str(i).zfill(2) + '_maxproj.tif'
            savefilez= tiff_folder + '/' + aliases[0] + '_S' + str(i).zfill(2) + '_maxprojz.tif'

            new_row = { 'imageID' : aliases[0] +'_S'+str(i).zfill(2) , 'expID' : metod[0] + '_' + magn[0]+'_' + str(ncells[0]),
                                    'absPath' : savefile, 'magn': magn[0], 'metod': metod[0],
                                         'um_per_pixel': um_per_pixel, 'Nx': Nx, 'Ny': Ny,
                                        'channels' :dict_channel, 'ncells': ncells[0]}
            df_IF= df_IF.append(new_row, ignore_index=True)

            if os.path.exists(savefile):
                print('File', savefile, 'already exists.')
            else:
                ## Create the max projection and save it
                img_tiff = np.zeros([Nc, Ny, Nx])
                indz_tiff = img_tiff.copy()
                for ch in range(Nc):

                    print(s,ch)
                    img_ch, indz = imu.maxprojection(folder+f, theS= s, theC =ch)
                    img_tiff[ch,:,:] =img_ch
                    indz_tiff[ch,:,:] =indz
                tifffile.imsave(savefile , img_tiff)
                tifffile.imsave(savefilez , indz_tiff)

            df_IF.to_json(target + '_data_PBS'+ gastru + '.json', orient='split')

    print(df_IF)

    tend= time.time()
    print(("time elapsed = ", (tend-tst)/60, "min"))

jv.kill_vm()
