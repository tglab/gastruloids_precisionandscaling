import os

import javabridge as jv
import bioformats
import src.image.imUtils as imu

import numpy as np
import pandas as pd
import tifffile

import matplotlib.pyplot as plt

import time

jv.start_vm(class_path=bioformats.JARS)
imu.init_logger()


#TODO: write the code that initialize the dataframe reading a worksheet with
# list of files and the corresponding alias/ parameters. or a type that givees the type of infos that should be read ?
# for instance: czi Nx, Ny, Nz, Ns, Nt, Nc etc

folder= '/mnt/pbf1/Melody/stacks_for_volume_leah/'

files=['96h_50_g1_substack.tif', '120h_50_g1_substack.tif' ]
magn= ['40x','40x']
metod = ['AS', 'AS']
aliases = ['50-96', '50-120']
ncells = [50,50]


# files=['RC01/10x-IFAS-01-Airyscan Processing-01.czi', 'RC01/10x-IFAS-02-Airyscan Processing-02.czi']
# magn= ['10x', '10x']
# metod = ['AS', 'AS']
# aliases = ['RC01-01', 'RC01-02', 'Vecta']
# ncells = [300, 300]

#files=['RC02/10x-IFAS-01-Airyscan Processing-01.czi', 'RC02/10x-IFAS-02-Airyscan Processing-02.czi',
#      'RC02/10x-IFAS-03-Airyscan Processing-03.czi', 'RC02/10x-IFAS-04-Airyscan Processing-04.czi']
# magn= ['10x', '10x', '10x', '10x']
# metod = ['AS', 'AS', 'AS', 'AS']
# aliases = ['RC02-01', 'RC02-02', 'RC02-03', 'RC02-04']
# ncells = [300, 300, 300,300]


# files=['RC03/10x-IFAS-01-Airyscan Processing-01.czi', 'RC03/10x-IFAS-02-Airyscan Processing-02.czi']
# magn= ['10x', '10x']
# metod = ['AS', 'AS']
# aliases = ['RC03-01', 'RC03-02']
# ncells = [300, 300]


dict_channel = {'DAPI':0 }

# ncells = [50,300,300,500,800,800,1200]

# aliases = ['bra1', 'bra2']
# ncells = [300,300]


target = '/mnt/home/results/experror/volumemeasurement/phall_50_Leah/'
if not os.path.exists(target):
    os.mkdir(target)

df_IF = pd.DataFrame(columns=['imageID', 'absPath',  'magn',  'metod' ,'um_per_pixel', 'Nx', 'Ny', 'channels', 'ncells'])

tst= time.time()

for i,f in enumerate(files):

    print('f', f)

    metadata = imu.get_metadata(folder + f)
    # Ns = metadata['Nseries']
    # print("# series = ", Ns)
    um_per_pixel = metadata['dx']
    Nc = metadata['Nch']
    Nx = metadata['Nx']
    Ny = metadata['Ny']

    tiff_folder  = os.path.join(target, aliases[i] )
    if not os.path.exists(tiff_folder):
        os.mkdir(tiff_folder)

    # for s in range(Ns):

    savefile = tiff_folder + '/' + aliases[i] + '_maxproj.tif'
    savefilez= tiff_folder + '/' + aliases[i] + '_maxprojz.tif'

    new_row = { 'imageID' : aliases[i] , 'expID' :aliases[i],
                            'absPath' : savefile, 'magn': magn[i], 'metod': metod[i],
                                 'um_per_pixel': um_per_pixel, 'Nx': Nx, 'Ny': Ny,
                                'channels' :dict_channel, 'ncells': ncells[i]}
    df_IF= df_IF.append(new_row, ignore_index=True)

    if os.path.exists(savefile):
        print('File', savefile, 'already exists.')
    else:
        ## Create the max projection and save it
        img_tiff = np.zeros([Nc, Ny, Nx])
        indz_tiff = img_tiff.copy()
        for ch in range(Nc):

            img_ch, indz = imu.maxprojection(folder+f, theC =ch)
            img_tiff[ch,:,:] =img_ch
            indz_tiff[ch,:,:] =indz
        tifffile.imsave(savefile , img_tiff)
        tifffile.imsave(savefilez , indz_tiff)

    df_IF.to_json(target + '_data_leah.json', orient='split')

print(df_IF)

tend= time.time()
print(("time elapsed = ", (tend-tst)/60, "min"))

jv.kill_vm()
